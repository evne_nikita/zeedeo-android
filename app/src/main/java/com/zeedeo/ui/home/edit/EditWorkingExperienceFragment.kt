package com.zeedeo.ui.home.edit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.adapter.recycler.home.EditWorkingExperienceRecyclerAdapter
import com.zeedeo.adapter.recycler.home.RemoveWorkingExperienceListener
import com.zeedeo.ui.dialog.DatePickerDialog
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.ui.home.edit.viewmodel.EditWorkingExperienceViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditWorkingExperienceViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CallDatePickerListener
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_quotes.backButton
import kotlinx.android.synthetic.main.fragment_edit_working_experience.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.IOException


class EditWorkingExperienceFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel
    private lateinit var viewModel: EditWorkingExperienceViewModel
    private val viewModelFactory: EditWorkingExperienceViewModelFactory by instance<EditWorkingExperienceViewModelFactory>()

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    private var selectedItem = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_working_experience, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        viewModel = viewModelFactory.create(EditWorkingExperienceViewModel::class.java)
        viewModel.setWorkingExperience(sharedViewModel.workingExperienceLiveData.value!!)
        viewModel.setImages(sharedViewModel.workingExperienceImagesLiveData.value!!)

        initRecycler()
    }

    override fun onStart() {
        super.onStart()
        viewModel.workingExperienceLiveData.observe(viewLifecycleOwner, Observer {
            (editWorkingExperienceRecyclerView.adapter as EditWorkingExperienceRecyclerAdapter).updateData(
                it
            )
        })
        viewModel.workingExperienceImagesLiveData.observe(viewLifecycleOwner, Observer {
            (editWorkingExperienceRecyclerView.adapter as EditWorkingExperienceRecyclerAdapter).loadCompanyLogos(
                it
            )
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.DATE_REQUEST_CODE) {
            data?.let {
                (editWorkingExperienceRecyclerView.adapter as EditWorkingExperienceRecyclerAdapter).setDatePicked(
                    selectedItem,
                    data.getStringExtra(Constants.DATE_KEY)!!
                )
            }
            // load image from gallery
        } else if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    loadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            photo?.let {
                loadImage(CommonUtils.getUriFromBitmap(it as Bitmap, requireContext()))
            }
            //image dialog result
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            // start gallery intent
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun loadImage(uri: Uri) {
        viewModel.setCompanyLogo(selectedItem, uri)
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
        addItemButton.setOnClickListener {
            viewModel.addWorkingExperience()
        }
    }

    private fun initRecycler() {
        editWorkingExperienceRecyclerView.layoutManager = LinearLayoutManager(context)
        editWorkingExperienceRecyclerView.adapter =
            EditWorkingExperienceRecyclerAdapter(workingExperience = sharedViewModel.workingExperienceLiveData.value!!,
                removeWorkingExperienceListener = object :
                    RemoveWorkingExperienceListener {
                    override fun onExperienceRemoved(position: Int) {
                        viewModel.removeWorkingExperience(position)
                    }
                },
                callDatePickerListener = object : CallDatePickerListener {
                    override fun datePickerCalled(position: Int) {
                        selectedItem = position
                        showDatePickerDialog()
                    }
                },
                addImageListener = object : AddImageListener {
                    override fun addImage(position: Int) {
                        selectedItem = position
                        showGetImageDialog()
                    }
                })
        (editWorkingExperienceRecyclerView.adapter as EditWorkingExperienceRecyclerAdapter).loadCompanyLogos(
            sharedViewModel.workingExperienceImagesLiveData.value!!
        )
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.uploadWorkingExperience(
                it.uid,
                viewModel.workingExperienceLiveData.value!!
            )
            firebaseUtils.updateWorkingExperienceImages(viewModel.workingExperienceLiveData.value!!)
        }
    }

    private fun showDatePickerDialog() {
        val dialog = DatePickerDialog(true)
        dialog.setTargetFragment(this, Constants.DATE_REQUEST_CODE)
        dialog.show(requireFragmentManager(), "datePickerDialog")
    }

    private fun showGetImageDialog() {
        val dialogFragment = PickImageDialog()
        dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
        dialogFragment.show(requireFragmentManager(), "Load image Dialog")
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }
}