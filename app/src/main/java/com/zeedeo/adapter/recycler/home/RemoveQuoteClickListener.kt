package com.zeedeo.adapter.recycler.home

interface RemoveQuoteClickListener {
    fun onRemoveClick(position: Int)
}