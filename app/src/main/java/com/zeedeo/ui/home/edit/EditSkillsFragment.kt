package com.zeedeo.ui.home.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.registration.EnterSkillsAdapter
import com.zeedeo.adapter.recycler.registration.RemoveSkillListener
import com.zeedeo.ui.home.edit.viewmodel.EditSkillsViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditSkillsViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_skills.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class EditSkillsFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel
    private lateinit var viewModel: EditSkillsViewModel
    private val viewModelFactory: EditSkillsViewModelFactory by instance<EditSkillsViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        initRecycler()
        viewModel = viewModelFactory.create(EditSkillsViewModel::class.java)
        viewModel.setSkills(sharedViewModel.skillsLiveData.value!!)
    }

    override fun onStart() {
        super.onStart()
        viewModel.skillsLiveData.observe(viewLifecycleOwner, Observer {
            (editSkillsRecyclerView.adapter as EnterSkillsAdapter).updateData(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
    }

    private fun initTextWatchers() {
        editSkillsEditText.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                viewModel.addSkill(view.text.toString())
                (editSkillsRecyclerView.adapter as EnterSkillsAdapter).addSkill(view.text.toString())
                view.text = ""
                view.requestFocus()
            }
            true
        }
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.uploadSkills(it.uid, viewModel.skillsLiveData.value!!)
        }
    }

    private fun initRecycler() {
        val chipsManager = ChipsLayoutManager.newBuilder(context)
            .setScrollingEnabled(true)
            .setOrientation(ChipsLayoutManager.HORIZONTAL)
            .build()
        editSkillsRecyclerView.layoutManager = chipsManager
        editSkillsRecyclerView.adapter =
            EnterSkillsAdapter(removeSkillListener = object : RemoveSkillListener {
                override fun onSkillRemoved(position: Int) {
                    viewModel.removeSkill(position)
                    (editSkillsRecyclerView.adapter as EnterSkillsAdapter).removeSkill(position)
                }
            })
    }
}