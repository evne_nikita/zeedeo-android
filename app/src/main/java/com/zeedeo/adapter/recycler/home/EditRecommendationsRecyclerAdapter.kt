package com.zeedeo.adapter.recycler.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.model.RecommendationModel
import com.zeedeo.utils.Constants

class EditRecommendationsRecyclerAdapter(
    private val recommendations: ArrayList<RecommendationModel> = arrayListOf(RecommendationModel()),
    private val removeRecommendationListener: RemoveRecommendationListener
) : RecyclerView.Adapter<EditRecommendationsRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_edit_recommendations, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recommendations.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun addRecommendation() {
        recommendations.add(RecommendationModel())
        notifyItemRangeChanged(0, recommendations.size)
    }

    fun removeRecommendation(position: Int) {
        if (position < recommendations.size) {
            recommendations.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val previewImageView =
            itemView.findViewById<AppCompatImageView>(R.id.editRecommendationsPreviewImageView)
        private val nameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.editRecommendationNameTextView)
        private val titleTextView =
            itemView.findViewById<AppCompatTextView>(R.id.editRecommendationsTitleTextView)
        private val removeRecommendationTextView =
            itemView.findViewById<AppCompatTextView>(R.id.removeRecommendationTextView)
        private val makeVisibleSwitch =
            itemView.findViewById<SwitchCompat>(R.id.visibleToOthersSwitch)

        fun bind(position: Int) {
            nameTextView.text = recommendations[position].name
            titleTextView.text = recommendations[position].title

            Glide.with(itemView.context).load(Constants.DEBUG_PROFILE_URL).into(previewImageView)

            removeRecommendationTextView.setOnClickListener {
                removeRecommendationListener.onRemoveRecommendation(position)
            }
            makeVisibleSwitch.setOnCheckedChangeListener { _, isChecked ->
                recommendations[position].visible = isChecked
            }
        }
    }

}