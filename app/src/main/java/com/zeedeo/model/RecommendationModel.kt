package com.zeedeo.model

data class RecommendationModel(
    var name: String = "",
    var title: String = "",
    var visible: Boolean = false
)