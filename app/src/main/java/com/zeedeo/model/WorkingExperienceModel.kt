package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class WorkingExperienceModel(
    @SerializedName("jobTitle")
    var jobTitle: String = "",
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("jobDescription")
    var jobDescription: String = "",
    @SerializedName("location")
    var location: String = "",
    @SerializedName("jobDates")
    var dates: String = "",
    @Transient
    var imageUrl: String = ""
) : Serializable