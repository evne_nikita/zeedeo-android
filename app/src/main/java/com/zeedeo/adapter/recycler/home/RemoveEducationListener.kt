package com.zeedeo.adapter.recycler.home

interface RemoveEducationListener {
    fun onRemoveEducation(position: Int)
}