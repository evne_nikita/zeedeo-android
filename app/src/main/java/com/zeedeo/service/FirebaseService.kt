package com.zeedeo.service

import com.google.firebase.messaging.FirebaseMessagingService
import com.zeedeo.utils.Constants
import com.zeedeo.utils.PreferencesStorage
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class FirebaseService : FirebaseMessagingService(), KodeinAware {
    override val kodein by kodein()
    private val storage: PreferencesStorage by instance<PreferencesStorage>()

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        storage.save(Constants.TOKEN_KEY, token)
    }
}