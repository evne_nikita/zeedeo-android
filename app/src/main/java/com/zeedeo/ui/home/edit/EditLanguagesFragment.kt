package com.zeedeo.ui.home.edit

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.adapter.recycler.home.EditLanguagesRecyclerAdapter
import com.zeedeo.adapter.recycler.home.RemoveLanguageListener
import com.zeedeo.ui.dialog.LanguageLevelDialog
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_languages.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class EditLanguagesFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel

    private var selectedItem = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_languages, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecycler()
        sharedViewModel = (activity as MainActivity).sharedViewModel
    }

    override fun onStart() {
        super.onStart()
        sharedViewModel.languagesLiveData.observe(viewLifecycleOwner, Observer {
            (editLanguagesRecyclerView.adapter as EditLanguagesRecyclerAdapter).updateData(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        loadData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.LANGUAGE_LEVEL_REQUEST_CODE) {
            data?.let {
                (editLanguagesRecyclerView.adapter as EditLanguagesRecyclerAdapter).setLevel(
                    selectedItem,
                    data.getStringExtra(Constants.LANGUAGE_LEVEL_KEY)!!
                )
            }
        }
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
        addMoreTextView.setOnClickListener {
            (editLanguagesRecyclerView.adapter as EditLanguagesRecyclerAdapter).addLanguage()
        }
    }

    private fun initRecycler() {
        editLanguagesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        editLanguagesRecyclerView.adapter =
            EditLanguagesRecyclerAdapter(removeLanguageListener = object : RemoveLanguageListener {
                override fun onRemoveLanguage(position: Int) {
                    (editLanguagesRecyclerView.adapter as EditLanguagesRecyclerAdapter).removeLanguage(
                        position
                    )
                }
            },
            languageClickListener = object: DateClickListener {
                override fun onClick(position: Int) {
                    selectedItem = position
                    showDialog()
                }
            })
    }

    private fun loadData() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.getUserLanguages(it.uid)
        }
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.uploadLanguages(
                it.uid,
                (editLanguagesRecyclerView.adapter as EditLanguagesRecyclerAdapter).languages
            )
        }
    }

    private fun showDialog() {
        val dialogFragment = LanguageLevelDialog()
        dialogFragment.setTargetFragment(this, Constants.LANGUAGE_LEVEL_REQUEST_CODE)
        fragmentManager?.let { dialogFragment.show(it, "Language Dialog") }
    }
}