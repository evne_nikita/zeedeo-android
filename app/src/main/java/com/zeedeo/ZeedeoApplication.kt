package com.zeedeo

import android.app.Application
import com.zeedeo.ui.auth.AuthViewModelFactory
import com.zeedeo.ui.home.edit.viewmodel.*
import com.zeedeo.ui.home.jobs.viewmodel.AllJobsViewModelFactory
import com.zeedeo.ui.home.jobs.viewmodel.AppliedJobsViewModelFactory
import com.zeedeo.ui.home.jobs.viewmodel.DismissedJobsViewModelFactory
import com.zeedeo.ui.home.jobs.viewmodel.SavedJobsViewModelFactory
import com.zeedeo.ui.main.MainActivityViewModelFactory
import com.zeedeo.ui.registration.RegistrationViewModelFactory
import com.zeedeo.utils.FirebaseUtils
import com.zeedeo.utils.PreferencesStorage
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class ZeedeoApplication : Application(), KodeinAware {
    override val kodein: Kodein = Kodein.lazy {
        import(androidXModule(this@ZeedeoApplication))

        bind() from singleton { PreferencesStorage(instance()) }
        bind() from singleton { FirebaseUtils() }

        bind() from provider { RegistrationViewModelFactory() }
        bind() from provider { MainActivityViewModelFactory() }
        bind() from provider { AuthViewModelFactory() }
        bind() from provider { EditAboutMeViewModelFactory() }
        bind() from provider { EditSkillsViewModelFactory() }
        bind() from provider { EditPersonalInterestsViewModelFactory() }
        bind() from provider { EditWorkingExperienceViewModelFactory() }
        bind() from provider { EditEducationViewModelFactory() }
        bind() from provider { EditInterestsViewModelFactory() }
        bind() from provider { SavedJobsViewModelFactory() }
        bind() from provider { AllJobsViewModelFactory() }
        bind() from provider { AppliedJobsViewModelFactory() }
        bind() from provider { DismissedJobsViewModelFactory() }
    }
}