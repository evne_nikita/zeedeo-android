package com.zeedeo.adapter.recycler.home

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.model.LanguageLevelModel

class EditLanguagesRecyclerAdapter(
    val languages: ArrayList<LanguageLevelModel> = ArrayList(),
    private val languageClickListener: DateClickListener,
    private val removeLanguageListener: RemoveLanguageListener
) :
    RecyclerView.Adapter<EditLanguagesRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_edit_languages, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return languages.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun setLevel(position: Int, level: String) {
        languages[position].level = level
        notifyItemChanged(position)
    }

    fun removeLanguage(position: Int) {
        languages.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeRemoved(position, languages.size)
    }

    fun addLanguage() {
        languages.add(LanguageLevelModel())
        notifyItemRangeChanged(0, languages.size)
    }

    fun updateData(newList: ArrayList<LanguageLevelModel>) {
        languages.clear()
        languages.addAll(newList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val languageEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editLanguagesLanguageEditText)
        private val languageLevelEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editLanguagesLanguageLevelEditText)
        private val removeLanguageButton =
            itemView.findViewById<AppCompatImageButton>(R.id.removeLanguageButton)
        private val delimiter = itemView.findViewById<View>(R.id.editLangaugesRecyclerDelimiter)

        fun bind() {
            languageEditText.setText(languages[adapterPosition].language)
            languageLevelEditText.setText(languages[adapterPosition].level)

            removeLanguageButton.setOnClickListener {
                removeLanguageListener.onRemoveLanguage(adapterPosition)
            }
            languageLevelEditText.setOnClickListener {
                languageClickListener.onClick(adapterPosition)
            }
            languageEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    languages[adapterPosition].language = s.toString()
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })


            if (adapterPosition < languages.size - 1) {
                delimiter.visibility = View.VISIBLE
            } else {
                delimiter.visibility = View.GONE
            }
        }
    }
}