package com.zeedeo.ui.registration

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.adapter.recycler.registration.LanguageLevelRecyclerAdapter
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.ui.dialog.LanguageLevelDialog
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.fragment_languages.*

class LanguagesFragment : Fragment() {

    private lateinit var viewModel: RegistrationViewModel

    private var selectedItem = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_languages, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
    }

    override fun onResume() {
        super.onResume()
        initCLickListeners()
        initRecycler()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.LANGUAGE_LEVEL_REQUEST_CODE) {
            data?.let {
                (languagesLevelRecyclerView.adapter as LanguageLevelRecyclerAdapter).setLevel(
                    selectedItem,
                    data.getStringExtra(Constants.LANGUAGE_LEVEL_KEY)!!
                )
            }
        }
    }

    private fun initRecycler() {
        languagesLevelRecyclerView.layoutManager = LinearLayoutManager(context)
        languagesLevelRecyclerView.adapter =
            LanguageLevelRecyclerAdapter(
                levelClickListener = object : DateClickListener {
                    override fun onClick(position: Int) {
                        selectedItem = position
                        showDialog()
                    }
                })
        (languagesLevelRecyclerView.adapter as LanguageLevelRecyclerAdapter).allFieldsFilled.observe(
            viewLifecycleOwner,
            Observer {
                if (it) {
                    viewModel.newUser.languages = (languagesLevelRecyclerView.adapter as LanguageLevelRecyclerAdapter).languageLevels
                }
                viewModel.setAllFieldsFilled(it)
            })
    }

    private fun initCLickListeners() {
        addMoreTextView.setOnClickListener {
            (languagesLevelRecyclerView.adapter as LanguageLevelRecyclerAdapter).addLanguage()
        }
    }

    private fun showDialog() {
        val dialogFragment = LanguageLevelDialog()
        dialogFragment.setTargetFragment(this, Constants.LANGUAGE_LEVEL_REQUEST_CODE)
        fragmentManager?.let { dialogFragment.show(it, "Language Dialog") }
    }
}