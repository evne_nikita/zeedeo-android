package com.zeedeo.adapter.recycler.registration

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.model.EducationHistoryModel
import com.zeedeo.utils.CommonUtils

class EducationHistoryRecyclerAdapter(
    val educationHistory: ArrayList<EducationHistoryModel> = arrayListOf(
        EducationHistoryModel()
    ),
    private val dateClickListener: DateClickListener,
    private val addImageListener: AddImageListener
) :
    RecyclerView.Adapter<EducationHistoryRecyclerAdapter.ViewHolder>() {

    private val _allFieldsFilled = MutableLiveData(false)
    val allFieldsFilled: LiveData<Boolean>
        get() = _allFieldsFilled

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_education_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return educationHistory.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun setEducationDates(position: Int, dates: String) {
        educationHistory[position].dates = dates
        notifyItemChanged(position)
        checkAllFieldsFilled()
    }

    fun addEducationHistory() {
        educationHistory.add(EducationHistoryModel())
        notifyItemRangeChanged(educationHistory.size - 1, educationHistory.size)
    }

    fun setCertificateImage(uri: Uri, position: Int) {
        educationHistory[position].imageUrl = uri.toString()
        notifyItemChanged(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val certificateNameEditText =
            itemView.findViewById<AppCompatEditText>(R.id.certificateEditText)
        private val educationLocationEditText =
            itemView.findViewById<AppCompatEditText>(R.id.educationLocationEditText)
        private val educationDurationTextView =
            itemView.findViewById<AppCompatTextView>(R.id.educationDurationTextView)
        private val educationAddPhotoTextView =
            itemView.findViewById<AppCompatTextView>(R.id.educationUploadPhotoTextView)
        private val educationAddedPhotoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.educationUploadedPhotoImageView)

        fun bind(position: Int) {
            initTextWatchers(position)
            initClickListeners(position)
            certificateNameEditText.setText(educationHistory[position].certificateName)
            educationLocationEditText.setText(educationHistory[position].educationLocation)
            educationDurationTextView.text = if (educationHistory[position].dates.isEmpty()) {
                itemView.context.resources.getString(R.string.datePickerHint)
            } else {
                educationHistory[position].dates
            }
            educationAddedPhotoImageView.setImageURI(Uri.parse(educationHistory[position].imageUrl))
        }

        private fun initClickListeners(position: Int) {
            educationDurationTextView.setOnClickListener {
                certificateNameEditText.clearFocus()
                educationLocationEditText.clearFocus()
                dateClickListener.onClick(position)
            }
            educationAddPhotoTextView.setOnClickListener {
                addImageListener.addImage(position)
            }
        }

        private fun initTextWatchers(position: Int) {
            certificateNameEditText.addTextChangedListener {
                educationHistory[position].certificateName = it.toString()
                checkAllFieldsFilled()
            }
            CommonUtils.clearFocusOnDone(certificateNameEditText)

            educationLocationEditText.addTextChangedListener {
                educationHistory[position].educationLocation = it.toString()
                checkAllFieldsFilled()
            }
            educationLocationEditText.setOnEditorActionListener { _, actionId, _ ->
                educationDurationTextView.callOnClick()
                false
            }
        }
    }

    private fun checkAllFieldsFilled() {
        for (item in educationHistory) {
            if (item.certificateName.isEmpty() || item.dates.isEmpty() || item.educationLocation.isEmpty()) {
                _allFieldsFilled.value = false
                return
            }
        }
        _allFieldsFilled.value = true
    }
}