package com.zeedeo.ui.home.jobs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.MarginItemDecoration
import com.zeedeo.adapter.recycler.home.jobs.AppliedJobsAdapter
import com.zeedeo.ui.home.jobs.viewmodel.AppliedJobsViewModel
import com.zeedeo.ui.home.jobs.viewmodel.AppliedJobsViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_applied_jobs.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AppliedJobsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var viewModel: AppliedJobsViewModel
    private val viewModelFactory: AppliedJobsViewModelFactory by instance<AppliedJobsViewModelFactory>()
    private lateinit var sharedViewModel: MainActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_applied_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelFactory.create(AppliedJobsViewModel::class.java)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        initRecycler()
        setObservers()
    }

    override fun onResume() {
        super.onResume()
        loadDebugData()
    }

    private fun initRecycler() {
        appliedJobsRecyclerView.layoutManager = LinearLayoutManager(context)
        appliedJobsRecyclerView.adapter =
            AppliedJobsAdapter()
        appliedJobsRecyclerView.addItemDecoration(
            MarginItemDecoration(
                CommonUtils.convertDpToPixel(
                    10,
                    requireContext()
                ).toInt()
            )
        )
    }

    private fun setObservers() {
        viewModel.appliedJobsLiveData.observe(viewLifecycleOwner, Observer {
            (appliedJobsRecyclerView.adapter as AppliedJobsAdapter).updateData(it)
            appliedJobsNumberTextView.text = getString(R.string.appliedJobsNumberText, it.size)
        })
    }

    private fun loadDebugData() {
        viewModel.setAppliedJobs(sharedViewModel.appliedJobs)
    }
}