package com.zeedeo.ui.home.jobs

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.yarolegovich.discretescrollview.transform.Pivot
import com.yarolegovich.discretescrollview.transform.ScaleTransformer
import com.zeedeo.R
import com.zeedeo.adapter.recycler.home.jobs.AllJobsAdapter
import com.zeedeo.adapter.recycler.home.jobs.DismissListener
import com.zeedeo.adapter.recycler.home.jobs.OnShareListener
import com.zeedeo.adapter.recycler.home.jobs.ShowDetailsListener
import com.zeedeo.model.JobModel
import com.zeedeo.ui.home.jobs.viewmodel.AllJobsViewModel
import com.zeedeo.ui.home.jobs.viewmodel.AllJobsViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import kotlinx.android.synthetic.main.fragment_all_jobs.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AllJobsFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private lateinit var viewModel: AllJobsViewModel
    private val viewModelFactory: AllJobsViewModelFactory by instance<AllJobsViewModelFactory>()
    private lateinit var sharedViewModel: MainActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_all_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelFactory.create(AllJobsViewModel::class.java)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        setObservers()
        initRecycler()
    }

    override fun onResume() {
        super.onResume()
        initDebugJobsList()
    }

    private fun initRecycler() {
        allJobsRecyclerView.adapter = AllJobsAdapter(shareListener = object : OnShareListener {
            override fun onShare(job: JobModel) {
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.putExtra(
                    Intent.EXTRA_TEXT,
                    getString(R.string.debugShareIntentMessage)
                )
                sendIntent.type = "text/plain"
                startActivity(sendIntent)
            }
        },
            dismissListener = object : DismissListener {
                override fun onJobDismissed(position: Int) {
                    sharedViewModel.dismissedJobs.add(viewModel.allJobsLiveData.value!![position])
                    viewModel.removeJob(position)
                    (allJobsRecyclerView.adapter as AllJobsAdapter).removeJob(position)
                }
            },
            showDetailsListener = object : ShowDetailsListener {
                override fun showDetails(position: Int, sharedElement: View) {
                    val extras = FragmentNavigatorExtras(
                        sharedElement to "jobDetailsTransition"
                    )

                    sharedViewModel.selectedJobModel =
                        viewModel.allJobsLiveData.value!![position]
                    findNavController().navigate(
                        R.id.action_homeFragment_to_jobDetailsFragment,
                        null,
                        null,
                        extras
                    )
                }
            })
        allJobsRecyclerView.setItemTransformer(
            ScaleTransformer.Builder()
                .setPivotY(Pivot(Pivot.AXIS_Y, -1))
                .build()
        )
    }

    private fun setObservers() {
        viewModel.allJobsLiveData.observe(viewLifecycleOwner, Observer {
            allJobsNumberTextView.text = getString(R.string.allJobsNumberText, it.size)
            (allJobsRecyclerView.adapter as AllJobsAdapter).updateData(it)
        })
    }


    private fun initDebugJobsList() {
        val newList = ArrayList<JobModel>()
        for (i in 0..8) {
            newList.add(
                JobModel(
                    companyName = "Company #$i",
                    detailsDescription = "description for job in company #$i",
                    salaryMin = 24,
                    salaryMax = 45,
                    location = "Bobrynets",
                    companyLocation = "Canada",
                    title = "Full stack developer"
                )
            )
        }
        viewModel.setJobs(newList)
    }
}