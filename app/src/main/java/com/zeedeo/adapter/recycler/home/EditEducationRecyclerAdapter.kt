package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.model.EducationHistoryModel
import com.zeedeo.utils.CallDatePickerListener

class EditEducationRecyclerAdapter(
    val education: ArrayList<EducationHistoryModel> = ArrayList(),
    private val removeEducationListener: RemoveEducationListener,
    private val datePickerListener: CallDatePickerListener,
    private val addImageListener: AddImageListener
) :
    RecyclerView.Adapter<EditEducationRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_edit_education, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return education.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(newList: ArrayList<EducationHistoryModel>) {
        education.clear()
        education.addAll(newList)
        notifyDataSetChanged()
    }

    fun setCertificateImage(uri: Uri, position: Int) {
        education[position].imageUrl = uri.toString()
        notifyItemChanged(position)
    }

    fun loadCertificates(images: ArrayList<Uri>) {
        for (i in 0 until images.size) {
            setCertificateImage(images[i], i)
        }
    }

    fun setDates(position: Int, dates: String) {
        education[position].dates = dates
        notifyItemChanged(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val certificateNameEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editEducationCertificateNameEditText)
        private val locationEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editEducationLocationEditText)
        private val datesEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editEducationDatesEditText)
        private val removeEducationButton =
            itemView.findViewById<AppCompatImageButton>(R.id.removeEducationButton)
        private val addPhotoTextView =
            itemView.findViewById<AppCompatTextView>(R.id.editEducationAddPhotoTextView)
        private val certificatePhotoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.editEducationCertificateImageView)
        private val delimiter = itemView.findViewById<View>(R.id.editEducationDelimiter)

        fun bind() {
            certificateNameEditText.setText(education[adapterPosition].certificateName)
            locationEditText.setText(education[adapterPosition].educationLocation)
            datesEditText.setText(education[adapterPosition].dates)
            Glide.with(itemView.context)
                .load(Uri.parse(education[adapterPosition].imageUrl))
                .into(certificatePhotoImageView)

            addPhotoTextView.visibility = if (education[adapterPosition].imageUrl.isEmpty()) {
                View.VISIBLE
            } else {
                View.GONE
            }

            initClickListeners()
            initTextWatchers()

            if (adapterPosition < education.size - 1) {
                delimiter.visibility = View.VISIBLE
            } else {
                delimiter.visibility = View.GONE
            }

        }

        private fun initClickListeners() {
            datesEditText.setOnClickListener {
                datePickerListener.datePickerCalled(adapterPosition)
            }
            removeEducationButton.setOnClickListener {
                removeEducationListener.onRemoveEducation(adapterPosition)
            }
            addPhotoTextView.setOnClickListener {
                addImageListener.addImage(adapterPosition)
            }
            certificatePhotoImageView.setOnClickListener {
                addImageListener.addImage(adapterPosition)
            }
        }

        private fun initTextWatchers() {
            certificateNameEditText.addTextChangedListener {
                education[adapterPosition].certificateName = it.toString()
            }
            locationEditText.addTextChangedListener {
                education[adapterPosition].educationLocation = it.toString()
            }
            locationEditText.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    datesEditText.callOnClick()
                }
                false
            }
        }
    }

}