package com.zeedeo.adapter.recycler.registration

interface RemoveSkillListener {
    fun onSkillRemoved(position: Int)
}