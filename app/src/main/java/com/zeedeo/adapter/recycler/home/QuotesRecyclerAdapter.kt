package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zeedeo.R
import com.zeedeo.utils.Constants

class QuotesRecyclerAdapter(private val quotesList: ArrayList<Uri> = ArrayList()) :
    RecyclerView.Adapter<QuotesRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_profile_quotes, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return quotesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateData(quotes: ArrayList<Uri>) {
        quotesList.clear()
        quotesList.addAll(quotes)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(position: Int) {
            val options: RequestOptions = RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .error(R.mipmap.ic_launcher_round)

            if (position >= quotesList.size) {
                Glide.with(itemView.context).load(Constants.DEBUG_QUOTES_URL).apply(options)
                    .into(itemView as AppCompatImageView)
            } else {
                Glide.with(itemView.context).load(quotesList[position]).apply(options)
                    .into(itemView as AppCompatImageView)
            }
        }
    }
}