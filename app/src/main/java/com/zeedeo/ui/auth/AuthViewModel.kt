package com.zeedeo.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.utils.Constants

class AuthViewModel : ViewModel() {

    private val _authFieldsValidLiveData = MutableLiveData(false)
    val authFieldsValidLiveData: LiveData<Boolean>
        get() = _authFieldsValidLiveData

    fun checkFieldsValid(passwordLength: Int) {
        _authFieldsValidLiveData.value = passwordLength > Constants.PASSWORD_MINIMAL_LENGTH
    }
}