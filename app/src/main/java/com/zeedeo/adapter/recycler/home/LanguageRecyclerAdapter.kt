package com.zeedeo.adapter.recycler.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.model.LanguageLevelModel

class LanguageRecyclerAdapter(private val languageLevels: ArrayList<LanguageLevelModel> = ArrayList()) :
    RecyclerView.Adapter<LanguageRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_profile_language, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return languageLevels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateData(newList: ArrayList<LanguageLevelModel>) {
        languageLevels.clear()
        languageLevels.addAll(newList)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val languageNameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.languageNameTextView)
        private val languageLevelTextView =
            itemView.findViewById<AppCompatTextView>(R.id.languageLevelTextView)

        fun bind(position: Int) {
            languageNameTextView.text = languageLevels[position].language
            languageLevelTextView.text = languageLevels[position].level
        }
    }

}