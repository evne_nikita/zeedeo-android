package com.zeedeo.adapter.recycler.home.jobs

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.model.JobModel
import com.zeedeo.utils.Constants

class SavedJobsRecyclerAdapter(private val jobs: ArrayList<JobModel> = ArrayList()) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val debugItemCount = 8

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_saved_job, parent, false)
        return SavedJobViewHolder(view)
    }

    override fun getItemCount(): Int {
        return debugItemCount
//        return jobs.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as SavedJobViewHolder).bind()
    }

    fun updateData(newList: ArrayList<JobModel>) {
        jobs.clear()
        jobs.addAll(newList)
        notifyDataSetChanged()
    }

    inner class SavedJobViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.savedJobCompanyLogo)
        private val companyNameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.savedJobCompanyNameTextView)
        private val jobDescriptionTextView =
            itemView.findViewById<AppCompatTextView>(R.id.savedJobJobDescriptionTextView)
        private val isFavoriteImageView =
            itemView.findViewById<AppCompatImageView>(R.id.savedJobFavoriteImageView)
        private val delimiter =
            itemView.findViewById<View>(R.id.savedJobDelimiter)

        fun bind() {
//            companyNameTextView.text = jobs[adapterPosition].title
//            jobDescriptionTextView.text = jobs[adapterPosition].detailsDescription
            companyNameTextView.text = "Company name"
            jobDescriptionTextView.text = "Nado vkalivat, no pain no gain"

            delimiter.visibility = if (adapterPosition < debugItemCount - 1) {
                View.VISIBLE
            } else {
                View.GONE
            }

            Glide.with(itemView.context).load(Constants.DEBUG_COMPANY_LOGO_URL).circleCrop()
                .into(companyLogoImageView)

            isFavoriteImageView.setColorFilter(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.white50
                ), android.graphics.PorterDuff.Mode.SRC_IN
            )
            isFavoriteImageView.setOnClickListener {
                (it as AppCompatImageView).setColorFilter(
                    Color.WHITE,
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            }
        }
    }

    inner class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            // set date to root view
        }
    }
}