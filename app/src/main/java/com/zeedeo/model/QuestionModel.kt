package com.zeedeo.model

import com.google.gson.annotations.SerializedName

data class QuestionModel(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("title")
    var title: String = ""
)