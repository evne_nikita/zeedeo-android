package com.zeedeo.ui.home.jobs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.JobModel

class AllJobsViewModel : ViewModel() {
    private val _allJobsLiveData = MutableLiveData(ArrayList<JobModel>())
    val allJobsLiveData: LiveData<ArrayList<JobModel>>
        get() = _allJobsLiveData

    fun setJobs(jobs: ArrayList<JobModel>) {
        _allJobsLiveData.value = jobs
    }

    fun removeJob(position: Int) {
        _allJobsLiveData.value?.removeAt(position)
    }
}