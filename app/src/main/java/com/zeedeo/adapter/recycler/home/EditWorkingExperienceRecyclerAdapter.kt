package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.model.WorkingExperienceModel
import com.zeedeo.utils.CallDatePickerListener

class EditWorkingExperienceRecyclerAdapter(
    val workingExperience: ArrayList<WorkingExperienceModel> = ArrayList(),
    private val removeWorkingExperienceListener: RemoveWorkingExperienceListener,
    private val callDatePickerListener: CallDatePickerListener,
    private val addImageListener: AddImageListener
) : RecyclerView.Adapter<EditWorkingExperienceRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_edit_working_experience, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return workingExperience.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(newList: ArrayList<WorkingExperienceModel>) {
        workingExperience.clear()
        workingExperience.addAll(newList)
        notifyDataSetChanged()
    }

    fun setDatePicked(position: Int, dates: String) {
        workingExperience[position].dates = dates
        notifyItemChanged(position)
    }

    fun setCompanyLogo(uri: Uri, position: Int) {
        workingExperience[position].imageUrl = uri.toString()
        notifyItemChanged(position)
    }

    fun loadCompanyLogos(images: ArrayList<Uri>) {
        for (i in 0 until images.size) {
            setCompanyLogo(images[i], i)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val jobTitleEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editWorkingExperienceJobTitleEditText)
        private val companyNameEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editWorkingExperienceCompanyNameEditText)
        private val locationEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editWorkingExperienceLocationEditText)
        private val datesEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editWorkingExperienceDatesEditText)
        private val descriptionEditText =
            itemView.findViewById<AppCompatEditText>(R.id.editWorkingExperienceDescriptionEditText)
        private val removeWorkingExperienceButton =
            itemView.findViewById<AppCompatImageButton>(R.id.removeWorkingExperienceButton)
        private val addLogoTextView =
            itemView.findViewById<AppCompatTextView>(R.id.editWorkingExperienceAddPhotoTextView)
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.editWorkingExperienceCompanyLogoImageView)

        fun bind() {
            jobTitleEditText.setText(workingExperience[adapterPosition].jobTitle)
            companyNameEditText.setText(workingExperience[adapterPosition].companyName)
            locationEditText.setText(workingExperience[adapterPosition].location)
            datesEditText.setText(workingExperience[adapterPosition].dates)
            descriptionEditText.setText(workingExperience[adapterPosition].jobDescription)
            if (workingExperience[adapterPosition].imageUrl.isNotEmpty()) {
                Glide.with(itemView.context)
                    .load(Uri.parse(workingExperience[adapterPosition].imageUrl))
                    .into(companyLogoImageView)
            }

            addLogoTextView.visibility =
                if (workingExperience[adapterPosition].imageUrl.isEmpty()) {
                    View.VISIBLE
                } else {
                    View.GONE
                }

            initCLickListeners()
            initTextWatchers()
        }

        private fun initCLickListeners() {
            datesEditText.setOnClickListener {
                callDatePickerListener.datePickerCalled(adapterPosition)
            }
            removeWorkingExperienceButton.setOnClickListener {
                removeWorkingExperienceListener.onExperienceRemoved(adapterPosition)
            }
            addLogoTextView.setOnClickListener {
                addImageListener.addImage(adapterPosition)
            }
            companyLogoImageView.setOnClickListener {
                addImageListener.addImage(adapterPosition)
            }
        }

        private fun initTextWatchers() {
            jobTitleEditText.addTextChangedListener {
                workingExperience[adapterPosition].jobTitle = it.toString()
            }
            companyNameEditText.addTextChangedListener {
                workingExperience[adapterPosition].companyName = it.toString()
            }
            descriptionEditText.addTextChangedListener {
                workingExperience[adapterPosition].jobDescription = it.toString()
            }
            locationEditText.addTextChangedListener {
                workingExperience[adapterPosition].location = it.toString()
            }
            locationEditText.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_NEXT) {
                    datesEditText.callOnClick()
                }
                false
            }
        }
    }
}