package com.zeedeo.adapter.recycler.home.jobs

import android.view.View

interface ShowDetailsListener {
    fun showDetails(position: Int, sharedElement: View)
}