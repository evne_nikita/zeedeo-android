package com.zeedeo.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.zeedeo.ui.home.jobs.AllJobsFragment
import com.zeedeo.ui.home.jobs.AppliedJobsFragment
import com.zeedeo.ui.home.jobs.DismissedJobsFragment
import com.zeedeo.ui.home.jobs.SavedJobsFragment

class MyJobsViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle): FragmentStateAdapter(fragmentManager, lifecycle) {
    private val itemCount = 4

    override fun getItemCount(): Int {
        return itemCount
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position) {
            0 -> fragment = AllJobsFragment()
            1 -> fragment = SavedJobsFragment()
            2 -> fragment = AppliedJobsFragment()
            3 -> fragment = DismissedJobsFragment()
        }
        return fragment!!
    }
}