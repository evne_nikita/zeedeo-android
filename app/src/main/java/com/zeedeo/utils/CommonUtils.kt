package com.zeedeo.utils

import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.provider.MediaStore
import android.util.DisplayMetrics
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import com.zeedeo.R
import java.io.ByteArrayOutputStream


object CommonUtils {
    fun hideKeyboard(context: Context, rootView: View) {
        with(context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager) {
            if (isActive) {
                hideSoftInputFromWindow(rootView.windowToken, 0)
            }
        }
    }

    fun forceKeyboard(context: Context, rootView: View) {
        val inputMethodManager =
            context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
        inputMethodManager!!.toggleSoftInputFromWindow(
            rootView.applicationWindowToken,
            InputMethodManager.SHOW_FORCED, 0
        )
    }

    fun convertDpToPixel(dp: Int, context: Context): Float {
        return dp * (context.resources
            .displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun clearFocusOnDone(editText: AppCompatEditText) {
        editText.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                view.clearFocus()
            }
            false
        }
    }

    fun getUriFromBitmap(bitmap: Bitmap, context: Context): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path: String = MediaStore.Images.Media.insertImage(
            context.contentResolver,
            bitmap,
            "Title",
            null
        )
        return Uri.parse(path)
    }

    fun createToast(title: String, subTitle: String = "", context: Context, parent: ViewGroup, offset: Int) {
        val toast = Toast.makeText(
            context,
            "",
            Toast.LENGTH_LONG
        )
        val view = LayoutInflater.from(context)
            .inflate(R.layout.window_popup, parent, false)
        view.findViewById<AppCompatTextView>(R.id.popupTitle).text = title
        view.findViewById<AppCompatTextView>(R.id.popupSubtitle).text = subTitle
        toast.view = view
        toast.setGravity(Gravity.FILL_HORIZONTAL or Gravity.TOP, 0, offset)
        toast.show()
    }

    fun createShareIntent(context: Context) {
        val sendIntent = Intent()
        sendIntent.action = Intent.ACTION_SEND
        sendIntent.putExtra(
            Intent.EXTRA_TEXT,
            context.getString(R.string.debugShareIntentMessage)
        )
        sendIntent.type = "text/plain"
        context.startActivity(sendIntent)
    }
}