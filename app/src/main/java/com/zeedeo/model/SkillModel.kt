package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class SkillModel(
    @SerializedName("title")
    val title: String = ""
): Serializable