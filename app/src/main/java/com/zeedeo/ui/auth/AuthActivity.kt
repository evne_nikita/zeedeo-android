package com.zeedeo.ui.auth

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.zeedeo.R
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.registration.RegistrationViewModel
import com.zeedeo.ui.registration.RegistrationViewModelFactory
import com.zeedeo.utils.FirebaseUtils
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AuthActivity : AppCompatActivity(), KodeinAware {
    override val kodein by kodein()
    lateinit var sharedRegistrationViewModel: RegistrationViewModel
    private val sharedRegistrationViewModelFactory: RegistrationViewModelFactory by instance<RegistrationViewModelFactory>()
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)
        sharedRegistrationViewModel =
            sharedRegistrationViewModelFactory.create(RegistrationViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        firebaseUtils.currentUserLiveData.observe(this@AuthActivity, Observer {
            if (it.uid.isNotEmpty()) {
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
            }
        })
    }
}