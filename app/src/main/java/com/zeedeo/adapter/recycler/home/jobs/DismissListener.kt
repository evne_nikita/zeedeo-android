package com.zeedeo.adapter.recycler.home.jobs

interface DismissListener {
    fun onJobDismissed(position: Int)
}