package com.zeedeo.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.zeedeo.R
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.dialog_pick_image.*

class PickImageDialog : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_pick_image, container, false)
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
    }

    private fun initClickListeners() {
        loadFromGalleryOption.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Constants.LOAD_IMAGE_TAG, Constants.LOAD_FROM_GALLERY_REQUEST_CODE)
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Constants.LOAD_IMAGE_REQUEST_CODE,
                intent
            )
            dismiss()
        }

        takePhotoOption.setOnClickListener {
            val intent = Intent()
            intent.putExtra(Constants.LOAD_IMAGE_TAG, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
            targetFragment!!.onActivityResult(
                targetRequestCode,
                Constants.LOAD_IMAGE_REQUEST_CODE,
                intent
            )
            dismiss()
        }

        cancelOption.setOnClickListener {
            dismiss()
        }
    }
}