package com.zeedeo.adapter.recycler.home.jobs

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.model.JobModel
import com.zeedeo.utils.Constants

class DismissedJobsAdapter(private val jobs: ArrayList<JobModel> = ArrayList()) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val debugItemCount = 13

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_dismissed_job, parent, false)
        return DismissedJobViewHolder(view)
    }

    override fun getItemCount(): Int {
//        return debugItemCount
        return jobs.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as DismissedJobViewHolder).bind()
    }

    fun updateData(newList: ArrayList<JobModel>) {
        jobs.clear()
        jobs.addAll(newList)
        notifyDataSetChanged()
    }

    inner class DismissedJobViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.dismissedJobCompanyLogo)
        private val companyNameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.dismissedJobCompanyNameTextView)
        private val jobDescriptionTextView =
            itemView.findViewById<AppCompatTextView>(R.id.dismissedJobJobDescriptionTextView)
        private val isFavoriteImageView =
            itemView.findViewById<AppCompatImageView>(R.id.dismissedJobFavoriteImageView)
        private val delimiter =
            itemView.findViewById<View>(R.id.dismissedJobDelimiter)

        fun bind() {
            companyNameTextView.text = jobs[adapterPosition].companyName
            jobDescriptionTextView.text = jobs[adapterPosition].detailsDescription
//            companyNameTextView.text = "Company name"
//            jobDescriptionTextView.text = "Nado vkalivat, no pain no gain"

            delimiter.visibility = if (adapterPosition < debugItemCount - 1) {
                View.VISIBLE
            } else {
                View.GONE
            }

            Glide.with(itemView.context).load(Constants.DEBUG_COMPANY_LOGO_URL).circleCrop()
                .into(companyLogoImageView)

            isFavoriteImageView.setColorFilter(
                ContextCompat.getColor(
                    itemView.context,
                    R.color.white50
                ), android.graphics.PorterDuff.Mode.SRC_IN
            )
            isFavoriteImageView.setOnClickListener {
                (it as AppCompatImageView).setColorFilter(
                    Color.WHITE,
                    android.graphics.PorterDuff.Mode.SRC_IN
                )
            }
        }
    }

    inner class DateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind() {
            // set date to root view
        }
    }
}