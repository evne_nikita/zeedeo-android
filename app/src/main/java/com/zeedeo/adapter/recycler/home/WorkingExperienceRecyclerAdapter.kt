package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.model.WorkingExperienceModel

class WorkingExperienceRecyclerAdapter(private val workingExperience: ArrayList<WorkingExperienceModel> = ArrayList()) :
    RecyclerView.Adapter<WorkingExperienceRecyclerAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_profile_working_experience, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return workingExperience.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateData(newList: ArrayList<WorkingExperienceModel>) {
        workingExperience.clear()
        workingExperience.addAll(newList)
        notifyDataSetChanged()
    }

    fun loadCompanyLogos(images: ArrayList<Uri>) {
        for (i in 0 until images.size) {
            if (i < workingExperience.size) {
                workingExperience[i].imageUrl = images[i].toString()
                notifyItemChanged(i)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.workingExperienceIconImageView)
        private val jobTitleTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingExperienceJobTitleTextView)
        private val companyNameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingExperienceCompanyNameTextView)
        private val workingDurationTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingExperienceDurationTextView)
        private val jobDetailsTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingExperienceDetailsTextView)

        fun bind(position: Int) {
            Glide.with(itemView.context).load(Uri.parse(workingExperience[position].imageUrl))
                .into(companyLogoImageView)

            jobTitleTextView.text = workingExperience[position].jobTitle
            companyNameTextView.text = workingExperience[position].companyName
            workingDurationTextView.text = workingExperience[position].dates
            jobDetailsTextView.text = workingExperience[position].jobDescription
        }
    }
}