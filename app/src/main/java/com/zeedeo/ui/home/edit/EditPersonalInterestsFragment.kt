package com.zeedeo.ui.home.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.zeedeo.R
import com.zeedeo.ui.home.edit.viewmodel.EditInterestsViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditInterestsViewModelFactory
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_education.backButton
import kotlinx.android.synthetic.main.fragment_edit_education.saveButton
import kotlinx.android.synthetic.main.fragment_edit_personal_interests.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class EditPersonalInterestsFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private lateinit var viewModel: EditInterestsViewModel
    private val viewModelFactory: EditInterestsViewModelFactory by instance<EditInterestsViewModelFactory>()
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_personal_interests, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelFactory.create(EditInterestsViewModel::class.java)
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
    }

    private fun initClickListeners() {
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initTextWatchers() {
        editPersonalInterestsEditText.addTextChangedListener {
            viewModel.setPersonalInterests(it.toString())
        }
    }

    private fun applyChanges() {
        firebaseUtils.uploadPersonalInterests(viewModel.personalInterestsLiveData.value!!)
    }
}