package com.zeedeo.ui.home.edit.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.UserModel

class EditPersonalInterestsViewModel : ViewModel() {

    private val _userInfoLiveData = MutableLiveData(UserModel())
    val userInfoLiveData: LiveData<UserModel>
        get() = _userInfoLiveData

    fun setUserInfo(user: UserModel) {
        _userInfoLiveData.value = user
    }

    fun setFirstName(name: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.firstName = name
            _userInfoLiveData.value = user
        }
    }

    fun setLastName(name: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.lastName = name
            _userInfoLiveData.value = user
        }
    }

    fun setEmail(email: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.email = email
            _userInfoLiveData.value = user
        }
    }

    fun setLocation(location: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.location = location
            _userInfoLiveData.value = user
        }
    }

    fun setTitle(title: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.title = title
            _userInfoLiveData.value = user
        }
    }

    fun setNationality(nationality: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.nationality = nationality
            _userInfoLiveData.value = user
        }
    }

    fun setBirthDate(date: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.birthDate = date
            _userInfoLiveData.value = user
        }
    }

    fun setEligibility(eligibility: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.eligibility = eligibility
            _userInfoLiveData.value = user
        }
    }

    fun setPhoneNumber(number: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.phoneNumber = number
            _userInfoLiveData.value = user
        }
    }

    fun setLinks(links: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.links = links
            _userInfoLiveData.value = user
        }
    }

    fun setSite(site: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.personalSite = site
            _userInfoLiveData.value = user
        }
    }

    fun setTwitter(twitter: String) {
        _userInfoLiveData.value?.let {
            val user = it
            user.twitter = twitter
            _userInfoLiveData.value = user
        }
    }

    fun setRate(rate: Float) {
        _userInfoLiveData.value?.let {
            val user = it
            user.rate = rate
            _userInfoLiveData.value = user
        }
    }

}