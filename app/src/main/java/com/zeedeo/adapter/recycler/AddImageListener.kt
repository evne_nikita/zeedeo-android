package com.zeedeo.adapter.recycler

interface AddImageListener {
    fun addImage(position: Int)
}