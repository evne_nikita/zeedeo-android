package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.model.EducationHistoryModel


class EducationHistoryRecyclerAdapter(private val educationHistory: ArrayList<EducationHistoryModel> = ArrayList()) :
    RecyclerView.Adapter<EducationHistoryRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_profile_education_history, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return educationHistory.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateData(newList: ArrayList<EducationHistoryModel>) {
        educationHistory.clear()
        educationHistory.addAll(newList)
        notifyDataSetChanged()
    }

    fun loadCertificates(images: ArrayList<Uri>) {
        for (i in 0 until images.size) {
            if (i < educationHistory.size) {
                educationHistory[i].imageUrl = images[i].toString()
                notifyItemChanged(i)
            }
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(position: Int) {
            Glide.with(itemView.context).load(Uri.parse(educationHistory[position].imageUrl))
                .into(itemView as AppCompatImageView)
        }
    }
}