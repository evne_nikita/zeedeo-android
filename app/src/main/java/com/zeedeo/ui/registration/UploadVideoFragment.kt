package com.zeedeo.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.zeedeo.R
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.ui.main.MainActivity
import kotlinx.android.synthetic.main.fragment_upload_video.*

class UploadVideoFragment : Fragment() {

    private lateinit var viewModel: RegistrationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_upload_video, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
    }

    override fun onResume() {
        super.onResume()
        viewModel.setAllFieldsFilled(false)
        addVideoOption.setOnClickListener {
            viewModel.setAllFieldsFilled(true)
        }
    }

}