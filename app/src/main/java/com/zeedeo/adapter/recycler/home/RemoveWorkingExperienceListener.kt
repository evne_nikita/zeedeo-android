package com.zeedeo.adapter.recycler.home

interface RemoveWorkingExperienceListener {
    fun onExperienceRemoved(position: Int)
}