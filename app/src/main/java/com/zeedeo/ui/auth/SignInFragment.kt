package com.zeedeo.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.zeedeo.R
import com.zeedeo.ui.registration.RegistrationViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import com.zeedeo.utils.isValidEmail
import kotlinx.android.synthetic.main.fragment_sign_in.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class SignInFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private lateinit var sharedRegistrationViewModel: RegistrationViewModel
    private lateinit var viewModel: AuthViewModel
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private val viewModelFactory: AuthViewModelFactory by instance<AuthViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedRegistrationViewModel = (activity as AuthActivity).sharedRegistrationViewModel
        viewModel = viewModelFactory.create(AuthViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        viewModel.authFieldsValidLiveData.observe(viewLifecycleOwner, Observer {
            signInButton.isEnabled = it
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
        changeSoftInputMode()
        signInButton.isEnabled = false
        viewModel.checkFieldsValid(signInPasswordEditText.text.toString().length)
    }

    private fun changeSoftInputMode() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    private fun initClickListeners() {
        registerOptionTextView.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_registrationFragment)
        }
        backButton.setOnClickListener {
            CommonUtils.hideKeyboard(requireContext(), signInFragment)
            findNavController().popBackStack()
        }
        signInButton.setOnClickListener {
            CommonUtils.hideKeyboard(requireContext(), signInFragment)
            firebaseUtils.signIn(
                signInEmailEditText.text.toString(),
                signInPasswordEditText.text.toString()
            )
        }
        forgotPasswordTextView.setOnClickListener {
            findNavController().navigate(R.id.action_signInFragment_to_forgotPasswordFragment)
        }
    }

    private fun initTextWatchers() {
        signInEmailEditText.addTextChangedListener {
            if (it.toString().isValidEmail()) {
                signInEmailInputLayout.setHintTextAppearance(R.style.validAuthInput)
                signInEmailInputLayout.hint = "Email"
            } else {
                signInEmailInputLayout.setHintTextAppearance(R.style.invalidAuthInput)
                signInEmailInputLayout.hint = "Invalid email"
            }
            signInButton.isEnabled = signFieldsValid()
        }

        signInPasswordEditText.addTextChangedListener {
            if (it.toString().length < Constants.PASSWORD_MINIMAL_LENGTH) {
                signInPasswordInputLayout.setHintTextAppearance(R.style.invalidAuthInput)
                signInPasswordInputLayout.hint = "Invalid password"
            } else {
                signInPasswordInputLayout.setHintTextAppearance(R.style.validAuthInput)
                signInPasswordInputLayout.hint = "Password"
            }
            signInButton.isEnabled = signFieldsValid()
        }
    }

    private fun signFieldsValid() =
        signInEmailEditText.text.toString().isValidEmail() &&
                signInPasswordEditText.text.toString().length >= Constants.PASSWORD_MINIMAL_LENGTH
}