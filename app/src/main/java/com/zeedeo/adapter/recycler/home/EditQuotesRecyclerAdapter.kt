package com.zeedeo.adapter.recycler.home

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants

class EditQuotesRecyclerAdapter(
    private val quotes: ArrayList<Uri> = ArrayList(),
    private val removeClickListener: RemoveQuoteClickListener
) :
    RecyclerView.Adapter<EditQuotesRecyclerAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_edit_quote, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return quotes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun updateData(newList: ArrayList<Uri>) {
        quotes.clear()
        quotes.addAll(newList)
        notifyDataSetChanged()
    }

    fun removeQuote(position: Int) {
        if (position < quotes.size) {
            quotes.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val quoteImageView = itemView.findViewById<AppCompatImageView>(R.id.quoteImageView)
        private val removeQuoteImageView =
            itemView.findViewById<AppCompatImageView>(R.id.deleteQuoteImageView)

        fun bind(position: Int) {
            Glide.with(itemView.context).load(quotes[position])
                .override(
                    CommonUtils.convertDpToPixel(
                        Constants.EDIT_QUOTES_ITEM_SIZE,
                        itemView.context
                    ).toInt()
                )
                .into(quoteImageView)

            removeQuoteImageView.setOnClickListener {
                removeClickListener.onRemoveClick(position)
            }
        }
    }
}