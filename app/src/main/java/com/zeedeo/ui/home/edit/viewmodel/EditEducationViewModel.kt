package com.zeedeo.ui.home.edit.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.EducationHistoryModel

class EditEducationViewModel : ViewModel() {
    private val _educationLiveData = MutableLiveData(ArrayList<EducationHistoryModel>())
    val educationLiveData: LiveData<ArrayList<EducationHistoryModel>>
        get() = _educationLiveData

    fun setEducation(newList: ArrayList<EducationHistoryModel>) {
        _educationLiveData.value = ArrayList(newList)
    }

    fun addEducation() {
        _educationImagesLiveData.value?.add(Uri.EMPTY)
        val newList = _educationLiveData.value
        newList?.add(EducationHistoryModel())
        _educationLiveData.value = newList
    }

    fun removeEducation(position: Int) {
        _educationImagesLiveData.value?.removeAt(position)
        val newList = _educationLiveData.value
        newList?.removeAt(position)
        _educationLiveData.value = newList
    }

    private val _educationImagesLiveData = MutableLiveData(ArrayList<Uri>())
    val educationImagesLiveData: LiveData<ArrayList<Uri>>
        get() = _educationImagesLiveData

    fun setImages(newList: ArrayList<Uri>) {
        _educationImagesLiveData.value = ArrayList(newList)
    }

    fun setCertificateImage(position: Int, uri: Uri) {
        val newList = _educationImagesLiveData.value
        if (position == newList?.size) {
            newList.add(uri)
        } else {
            newList!![position] = uri
        }
        _educationImagesLiveData.value = newList
    }
}