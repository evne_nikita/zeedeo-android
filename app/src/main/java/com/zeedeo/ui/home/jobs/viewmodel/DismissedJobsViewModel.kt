package com.zeedeo.ui.home.jobs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.JobModel

class DismissedJobsViewModel : ViewModel() {
    private val _dismissedJobsLiveData = MutableLiveData(ArrayList<JobModel>())
    val dismissedJobsLiveData: LiveData<ArrayList<JobModel>>
        get() = _dismissedJobsLiveData

    fun setJobs(jobs: ArrayList<JobModel>) {
        _dismissedJobsLiveData.value = jobs
    }

    fun removeJob(position: Int) {
        _dismissedJobsLiveData.value?.removeAt(position)
    }
}