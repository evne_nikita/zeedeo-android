package com.zeedeo.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.zeedeo.ui.home.FeedFragment
import com.zeedeo.ui.home.MessagesFragment
import com.zeedeo.ui.home.MyJobsFragment
import com.zeedeo.ui.home.MyNetworkFragment

class HomeViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {

    private val itemCount = 4

    override fun getItemCount(): Int {
        return itemCount
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = FeedFragment()
            1 -> fragment = MyJobsFragment()
            2 -> fragment = MyNetworkFragment()
            3 -> fragment = MessagesFragment()
        }
        return fragment!!
    }
}