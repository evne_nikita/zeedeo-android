package com.zeedeo.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.zeedeo.ui.registration.LanguagesFragment
import com.zeedeo.ui.registration.*

class RegistrationViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val itemCount = 6

    override fun getItemCount(): Int {
        return itemCount
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when (position) {
            0 -> fragment = CompleteProfileFragment()
            1 -> fragment = UploadVideoFragment()
            2 -> fragment = EnterSkillsFragment()
            3 -> fragment =
                WorkingExperienceFragment()
            4 -> fragment = EducationHistoryFragment()
            5 -> fragment = LanguagesFragment()
        }
        return fragment!!
    }
}