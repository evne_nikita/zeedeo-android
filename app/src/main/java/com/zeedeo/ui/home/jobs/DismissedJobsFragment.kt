package com.zeedeo.ui.home.jobs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.MarginItemDecoration
import com.zeedeo.adapter.recycler.home.jobs.DismissedJobsAdapter
import com.zeedeo.ui.home.jobs.viewmodel.DismissedJobsViewModel
import com.zeedeo.ui.home.jobs.viewmodel.DismissedJobsViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_dismissed_jobs.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class DismissedJobsFragment : Fragment(), KodeinAware {

    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var viewModel: DismissedJobsViewModel
    private val viewModelFactory: DismissedJobsViewModelFactory by instance<DismissedJobsViewModelFactory>()
    private lateinit var sharedViewModel: MainActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dismissed_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelFactory.create(DismissedJobsViewModel::class.java)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        initRecycler()
        setObservers()
    }

    override fun onResume() {
        super.onResume()
        loadDebugData()
    }

    private fun initRecycler() {
        dismissedJobsRecyclerView.layoutManager = LinearLayoutManager(context)
        dismissedJobsRecyclerView.adapter =
            DismissedJobsAdapter()
        dismissedJobsRecyclerView.addItemDecoration(
            MarginItemDecoration(
                CommonUtils.convertDpToPixel(
                    10,
                    requireContext()
                ).toInt()
            )
        )
    }

    private fun setObservers() {
        viewModel.dismissedJobsLiveData.observe(viewLifecycleOwner, Observer {
            (dismissedJobsRecyclerView.adapter as DismissedJobsAdapter).updateData(it)
            dismissedJobsNumberTextView.text = getString(R.string.dismissedJobsNumberText, it.size)
        })
    }

    private fun loadDebugData() {
        viewModel.setJobs(sharedViewModel.dismissedJobs)
    }
}