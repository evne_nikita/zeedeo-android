package com.zeedeo.ui.home.jobs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.MarginItemDecoration
import com.zeedeo.adapter.recycler.home.jobs.SavedJobsRecyclerAdapter
import com.zeedeo.ui.home.jobs.viewmodel.SavedJobsViewModel
import com.zeedeo.ui.home.jobs.viewmodel.SavedJobsViewModelFactory
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_saved_jobs.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class SavedJobsFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var viewModel: SavedJobsViewModel
    private val viewModelFactory: SavedJobsViewModelFactory by instance<SavedJobsViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_saved_jobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModelFactory.create(SavedJobsViewModel::class.java)
        initRecycler()
        setObservers()
    }

    private fun initRecycler() {
        savedJobsRecyclerView.layoutManager = LinearLayoutManager(context)
        savedJobsRecyclerView.adapter =
            SavedJobsRecyclerAdapter()
        savedJobsRecyclerView.addItemDecoration(
            MarginItemDecoration(
                CommonUtils.convertDpToPixel(
                    10,
                    requireContext()
                ).toInt()
            )
        )
    }

    private fun setObservers() {
        viewModel.savedJobLiveData.observe(viewLifecycleOwner, Observer {
            (savedJobsRecyclerView.adapter as SavedJobsRecyclerAdapter).updateData(it)
            savedJobsNumberTextView.text = getString(R.string.savedJobsNumberText, it.size)
        })
    }
}