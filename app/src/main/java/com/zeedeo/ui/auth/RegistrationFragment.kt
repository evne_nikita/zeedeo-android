package com.zeedeo.ui.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.zeedeo.R
import com.zeedeo.ui.registration.RegistrationViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.isValidEmail
import kotlinx.android.synthetic.main.fragment_registration.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class RegistrationFragment : Fragment(), KodeinAware {
    override val kodein by kodein()
    private lateinit var regisrationViewModel: RegistrationViewModel
    private lateinit var viewModel: AuthViewModel
    private val viewModelFactory: AuthViewModelFactory by instance<AuthViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_registration, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        regisrationViewModel = (activity as AuthActivity).sharedRegistrationViewModel
        viewModel = viewModelFactory.create(AuthViewModel::class.java)
        registerButton.isEnabled = false
    }

    override fun onStart() {
        super.onStart()
        viewModel.authFieldsValidLiveData.observe(viewLifecycleOwner, Observer {
            registerButton.isEnabled = it
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
        changeInputSoftMode()
    }

    private fun initClickListeners() {
        signInOptionTextView.setOnClickListener {
            findNavController().navigate(R.id.action_registrationFragment_to_signInFragment)
        }
        registerButton.setOnClickListener {
            CommonUtils.hideKeyboard(it.context, registrationFragment)
            findNavController().navigate(R.id.action_registrationFragment_to_accountCreatedFragment)
        }
    }

    private fun changeInputSoftMode() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
    }

    private fun initTextWatchers() {
        registrationEmailEditText.addTextChangedListener {
            if (it.toString().isValidEmail()) {
                registrationEmailInputLayout.setHintTextAppearance(R.style.validAuthInput)
                registrationEmailInputLayout.hint = "Email"
            } else {
                registrationEmailInputLayout.setHintTextAppearance(R.style.invalidAuthInput)
                registrationEmailInputLayout.hint = "Invalid email"
            }
            regisrationViewModel.newUser.email = it.toString()
            registerButton.isEnabled = signFieldsValid()
        }
        registrationPasswordEditText.addTextChangedListener {
            if (it.toString().length < Constants.PASSWORD_MINIMAL_LENGTH) {
                registrationPasswordInputLayout.setHintTextAppearance(R.style.invalidAuthInput)
                registrationPasswordInputLayout.hint = "Invalid password"
            } else {
                registrationPasswordInputLayout.setHintTextAppearance(R.style.validAuthInput)
                registrationPasswordInputLayout.hint = "Password"
            }
            regisrationViewModel.newUser.password = it.toString()
            registerButton.isEnabled = signFieldsValid()
        }
    }

    private fun signFieldsValid() =
        regisrationViewModel.newUser.email.isValidEmail() &&
                regisrationViewModel.newUser.password.length >= Constants.PASSWORD_MINIMAL_LENGTH
}