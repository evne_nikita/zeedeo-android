package com.zeedeo.utils

interface CallDatePickerListener {
    fun datePickerCalled(position: Int)
}