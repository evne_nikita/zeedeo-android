package com.zeedeo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.zeedeo.R
import com.zeedeo.adapter.viewpager.MyJobsViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_my_jobs.*

class MyJobsFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_jobs, container, false)
    }

    override fun onResume() {
        super.onResume()
        initViewPager()
    }

    private fun initViewPager() {
        myJobsViewPager.isUserInputEnabled = false
        myJobsViewPager.adapter = MyJobsViewPagerAdapter(childFragmentManager, lifecycle)
        TabLayoutMediator(
            myJobsTabLayout,
            myJobsViewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> tab.text = "All"
                    1 -> tab.text = "Saved"
                    2 -> tab.text = "Applied"
                    3 -> tab.text = "Dismissed"
                }
            }).attach()
    }
}