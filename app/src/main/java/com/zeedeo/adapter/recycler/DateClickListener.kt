package com.zeedeo.adapter.recycler

interface DateClickListener {
    fun onClick(position: Int)
}