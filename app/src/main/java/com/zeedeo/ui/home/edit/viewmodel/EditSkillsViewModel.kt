package com.zeedeo.ui.home.edit.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.SkillModel

class EditSkillsViewModel : ViewModel() {
    private val _skillsLiveData = MutableLiveData(ArrayList<SkillModel>())
    val skillsLiveData: LiveData<ArrayList<SkillModel>>
        get() = _skillsLiveData

    fun setSkills(skills: ArrayList<SkillModel>) {
        _skillsLiveData.value = ArrayList(skills)
    }

    fun addSkill(skill: String) {
        _skillsLiveData.value?.add(SkillModel(skill))
    }

    fun removeSkill(position: Int) {
        _skillsLiveData.value?.removeAt(position)
    }
}