package com.zeedeo.ui.registration

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import com.zeedeo.R
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.ui.dialog.DatePickerDialog
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.fragment_complete_profile.*
import java.io.IOException


class CompleteProfileFragment : Fragment() {

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    private lateinit var viewModel: RegistrationViewModel

    private var datePicked = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_complete_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // get picked date
        if (requestCode == Constants.DATE_REQUEST_CODE) {
            data?.let {
                registrationBirthDateTextView.text = it.getStringExtra(Constants.DATE_KEY)
                viewModel.newUser.birthDate = it.getStringExtra(Constants.DATE_KEY) ?: ""
                datePicked = true
                viewModel.setAllFieldsFilled(allFieldsFilled())
            }
            // load image from gallery
        } else if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    loadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            photo?.let {
                loadImage(CommonUtils.getUriFromBitmap(it as Bitmap, requireContext()))
            }
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun loadImage(uri: Uri) {
        viewModel.newUser.profileImage = uri.toString()
        completeProfileUploadedPhotoImageView.setImageURI(uri)
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }


    private fun initClickListeners() {
        registrationBirthDateTextView.setOnClickListener {
            showDatePickerDialog()
        }
        completeProfileUploadPhotoTextView.setOnClickListener {
            showGetImageDialog()
        }
    }

    private fun initTextWatchers() {
        registrationFirstNameEditText.addTextChangedListener {
            viewModel.newUser.firstName = it.toString()
            viewModel.setAllFieldsFilled(allFieldsFilled())
        }
        registrationFirstNameEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationLastNameEditText.requestFocus()
            }
            false
        }

        registrationLastNameEditText.addTextChangedListener {
            viewModel.newUser.lastName = it.toString()
            viewModel.setAllFieldsFilled(allFieldsFilled())
        }
        registrationLastNameEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationLocationEditText.requestFocus()
            }
            false
        }

        registrationLocationEditText.addTextChangedListener {
            viewModel.newUser.location = it.toString()
        }
        registrationLocationEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationJobTitleEditText.requestFocus()
            }
            false
        }

        registrationJobTitleEditText.addTextChangedListener {
            viewModel.newUser.title = it.toString()
        }
        registrationJobTitleEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationAboutYouEditText.requestFocus()
            }
            false
        }

        registrationAboutYouEditText.addTextChangedListener {
            viewModel.newUser.about = it.toString()
        }
        registrationAboutYouEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                registrationNationalityEditText.requestFocus()
            }
            false
        }

        registrationNationalityEditText.addTextChangedListener {
            viewModel.newUser.nationality = it.toString()
        }
        registrationNationalityEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                CommonUtils.hideKeyboard(requireContext(), completeProfileFragment)
                registrationBirthDateTextView.callOnClick()
            }
            false
        }
    }

    private fun allFieldsFilled(): Boolean {
        return (!registrationFirstNameEditText.text.isNullOrEmpty() &&
                !registrationLastNameEditText.text.isNullOrEmpty() &&
                !registrationLocationEditText.text.isNullOrEmpty() &&
                !registrationJobTitleEditText.text.isNullOrEmpty() &&
                !registrationAboutYouEditText.text.isNullOrEmpty() &&
                !registrationNationalityEditText.text.isNullOrEmpty() &&
                datePicked
                )
    }

    private fun showDatePickerDialog() {
        val dialog = DatePickerDialog(false)
        dialog.setTargetFragment(this, Constants.DATE_REQUEST_CODE)
        dialog.show(requireFragmentManager(), "datePickerDialog")
    }

    private fun showGetImageDialog() {
        val dialogFragment = PickImageDialog()
        dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
        dialogFragment.show(requireFragmentManager(), "Load image Dialog")
    }
}