package com.zeedeo.adapter.recycler.home.jobs

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ScrollView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.zeedeo.R
import com.zeedeo.model.JobModel
import com.zeedeo.utils.Constants

class AllJobsAdapter(
    private val jobs: ArrayList<JobModel> = ArrayList(),
    private val shareListener: OnShareListener,
    private val dismissListener: DismissListener,
    private val showDetailsListener: ShowDetailsListener
) :
    RecyclerView.Adapter<AllJobsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_all_jobs, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return jobs.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(newList: ArrayList<JobModel>) {
        jobs.clear()
        jobs.addAll(newList)
        notifyDataSetChanged()
    }

    fun removeJob(position: Int) {
        jobs.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeRemoved(0, jobs.size - 1)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val previewImageView =
            itemView.findViewById<AppCompatImageView>(R.id.allJobsPreviewImageView)
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.allJobsCompanyLogoImageView)
        private val companyNameTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsCompanyNameTextView)
        private val companyDescriptionTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsCompanyDescriptionTextView)
        private val companyLocationTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsCompanyLocationTextView)
        private val jobTitleTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsJobTitleTextView)
        private val jobLocationTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsJobLocationTextView)
        private val salaryTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsSalaryTextView)
        private val shareButton =
            itemView.findViewById<AppCompatImageView>(R.id.allJobsShareIcon)
        private val dismissTextView =
            itemView.findViewById<AppCompatTextView>(R.id.allJobsDismissTextView)
        private val showDetailsButton =
            itemView.findViewById<AppCompatButton>(R.id.allJobsShowDetailsButton)

        fun bind() {
            (itemView as ScrollView).scrollTo(0, 0)
            loadImages()
            displayData()
            initOnClickListeners()
        }

        private fun loadImages() {
            var options = RequestOptions()
            options = options.transforms(CenterCrop(), RoundedCorners(50))
            Glide.with(itemView.context).load(Constants.DEBUG_JOB_PREVIEW_URL)
                .apply(options)
                .into(previewImageView)
            Glide.with(itemView.context).load(Constants.DEBUG_COMPANY_LOGO_URL)
                .circleCrop()
                .into(companyLogoImageView)
        }

        private fun displayData() {
            companyNameTextView.text = jobs[adapterPosition].companyName
            companyDescriptionTextView.text = jobs[adapterPosition].detailsDescription
            companyLocationTextView.text = jobs[adapterPosition].companyLocation
            jobTitleTextView.text = jobs[adapterPosition].title
            jobLocationTextView.text = jobs[adapterPosition].location
            salaryTextView.text = itemView.context.getString(
                R.string.salaryPlaceHolder,
                jobs[adapterPosition].salaryMin,
                jobs[adapterPosition].salaryMax
            )
        }

        private fun initOnClickListeners() {
            shareButton.setOnClickListener {
                shareListener.onShare(JobModel())
            }
            dismissTextView.setOnClickListener {
                dismissListener.onJobDismissed(adapterPosition)
            }
            showDetailsButton.setOnClickListener {
                itemView.transitionName = "jobDetailsTransition"
                showDetailsListener.showDetails(adapterPosition, itemView)
            }
        }
    }
}