package com.zeedeo.ui.home.edit.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.WorkingExperienceModel

class EditWorkingExperienceViewModel : ViewModel() {
    private val _workingExperienceLiveData = MutableLiveData(ArrayList<WorkingExperienceModel>())
    val workingExperienceLiveData: LiveData<ArrayList<WorkingExperienceModel>>
        get() = _workingExperienceLiveData

    fun setWorkingExperience(newList: ArrayList<WorkingExperienceModel>) {
        _workingExperienceLiveData.value = ArrayList(newList)
    }

    fun addWorkingExperience() {
        val newList = _workingExperienceLiveData.value
        newList?.add(WorkingExperienceModel())
        _workingExperienceImagesLiveData.value?.add(Uri.EMPTY)
        _workingExperienceLiveData.value = newList
    }

    fun removeWorkingExperience(position: Int) {
        _workingExperienceImagesLiveData.value?.removeAt(position)
        val newList = _workingExperienceLiveData.value
        newList?.removeAt(position)
        _workingExperienceLiveData.value = newList
    }

    private val _workingExperienceImagesLiveData = MutableLiveData(ArrayList<Uri>())
    val workingExperienceImagesLiveData: LiveData<ArrayList<Uri>>
        get() = _workingExperienceImagesLiveData

    fun setImages(images: ArrayList<Uri>) {
        _workingExperienceImagesLiveData.value = ArrayList(images)
    }

    fun setCompanyLogo(position: Int, uri: Uri) {
        val newList = _workingExperienceImagesLiveData.value
        if (position == newList?.size) {
            newList.add(uri)
        } else {
            newList!![position] = uri
        }
        _workingExperienceImagesLiveData.value = newList
    }
}