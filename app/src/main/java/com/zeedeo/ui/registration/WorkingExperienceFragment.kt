package com.zeedeo.ui.registration

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.adapter.recycler.registration.WorkingExperienceRecyclerAdapter
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.ui.dialog.DatePickerDialog
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.fragment_working_experience.*
import java.io.IOException

class WorkingExperienceFragment : Fragment() {

    private var selectedItem = 0
    private lateinit var viewModel: RegistrationViewModel

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_working_experience, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
        initRecyclerView()
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        viewModel.setAllFieldsFilled(false)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.DATE_REQUEST_CODE) {
            data?.let {
                (workingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).setDatePicked(
                    selectedItem,
                    data.getStringExtra(Constants.DATE_KEY)!!
                )
            }
            // load from gallery
        } else if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    loadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            photo?.let {
                loadImage(CommonUtils.getUriFromBitmap(it as Bitmap, requireContext()))
            }
            // image dialog result
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            // start gallery intent
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun loadImage(uri: Uri) {
        (workingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).setCompanyLogo(
            uri,
            selectedItem
        )
    }

    private fun initRecyclerView() {
        workingExperienceRecyclerView.layoutManager = LinearLayoutManager(context)
        workingExperienceRecyclerView.adapter =
            WorkingExperienceRecyclerAdapter(
                dateClickListener = object : DateClickListener {
                    override fun onClick(position: Int) {
                        selectedItem = position
                        showDatePickerDialog()
                    }
                },
                addImageListener = object : AddImageListener {
                    override fun addImage(position: Int) {
                        selectedItem = position
                        showGetImageDialog()
                    }
                })
        (workingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).allFieldsFilled.observe(
            viewLifecycleOwner,
            Observer {
                if (it) {
                    viewModel.newUser.workingExperience =
                        (workingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).workingExperience
                }
                viewModel.setAllFieldsFilled(it)
            })
    }

    private fun showDatePickerDialog() {
        val dialog = DatePickerDialog(true)
        dialog.setTargetFragment(this, Constants.DATE_REQUEST_CODE)
        dialog.show(requireFragmentManager(), "datePickerDialog")
    }

    private fun initClickListeners() {
        addMoreExperienceTextView.setOnClickListener {
            (workingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).addExperience()
        }
    }

    private fun showGetImageDialog() {
        val dialogFragment = PickImageDialog()
        dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
        dialogFragment.show(requireFragmentManager(), "Load image Dialog")
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }
}