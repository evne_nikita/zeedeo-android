package com.zeedeo.adapter.recycler.registration

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.model.WorkingExperienceModel
import com.zeedeo.utils.CommonUtils

class WorkingExperienceRecyclerAdapter(
    val workingExperience: ArrayList<WorkingExperienceModel> = arrayListOf(
        WorkingExperienceModel()
    ),
    private val dateClickListener: DateClickListener,
    private val addImageListener: AddImageListener
) :
    RecyclerView.Adapter<WorkingExperienceRecyclerAdapter.ViewHolder>() {

    private val _allFieldsFilled = MutableLiveData(false)
    val allFieldsFilled: LiveData<Boolean>
        get() = _allFieldsFilled

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_working_experience, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return workingExperience.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun setDatePicked(position: Int, dates: String) {
        workingExperience[position].dates = dates
        notifyItemChanged(position)
        checkAllFieldsFilled()
    }

    fun addExperience() {
        workingExperience.add(WorkingExperienceModel())
        notifyItemRangeChanged(workingExperience.size - 1, 1)
    }

    fun setCompanyLogo(uri: Uri, position: Int) {
        workingExperience[position].imageUrl = uri.toString()
        notifyItemChanged(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val jobTitleEditText =
            itemView.findViewById<AppCompatEditText>(R.id.addExperienceJobTitleEditText)
        private val companyNameEditText =
            itemView.findViewById<AppCompatEditText>(R.id.addExperienceCompanyEditText)
        private val jobDescriptionEditText =
            itemView.findViewById<AppCompatEditText>(R.id.jobDescriptionEditText)
        private val datesTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingDatesTextView)
        private val addPhotoTextView =
            itemView.findViewById<AppCompatTextView>(R.id.workingExperienceAddPhotoTextView)
        private val companyLogoImageView =
            itemView.findViewById<AppCompatImageView>(R.id.workingExperienceUploadedPhotoImageView)

        fun bind(position: Int) {
            initTextWatchers(position)
            initClickListeners(position)

            jobTitleEditText.setText(workingExperience[position].jobTitle)
            companyNameEditText.setText(workingExperience[position].companyName)
            jobDescriptionEditText.setText(workingExperience[position].jobDescription)
            datesTextView.text = if (workingExperience[position].dates.isNotEmpty()) {
                workingExperience[position].dates
            } else {
                itemView.context.resources.getString(R.string.datePickerHint)
            }
            companyLogoImageView.setImageURI(Uri.parse(workingExperience[position].imageUrl))
        }

        private fun initClickListeners(position: Int) {
            datesTextView.setOnClickListener {
                jobTitleEditText.clearFocus()
                companyNameEditText.clearFocus()
                jobDescriptionEditText.clearFocus()
                dateClickListener.onClick(position)
            }
            addPhotoTextView.setOnClickListener {
                addImageListener.addImage(position)
            }
        }

        private fun initTextWatchers(position: Int) {
            jobTitleEditText.addTextChangedListener {
                workingExperience[position].jobTitle = it.toString()
                checkAllFieldsFilled()
            }
            CommonUtils.clearFocusOnDone(jobTitleEditText)

            companyNameEditText.addTextChangedListener {
                workingExperience[position].companyName = it.toString()
                checkAllFieldsFilled()
            }
            companyNameEditText.setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                    datesTextView.callOnClick()
                }
                true
            }

            jobDescriptionEditText.addTextChangedListener {
                workingExperience[position].jobDescription = it.toString()
                checkAllFieldsFilled()
            }

            CommonUtils.clearFocusOnDone(jobDescriptionEditText)
        }
    }

    fun checkAllFieldsFilled() {
        for (item in workingExperience) {
            if (item.dates.isEmpty() || item.jobTitle.isEmpty() || item.jobDescription.isEmpty() || item.companyName.isEmpty()) {
                _allFieldsFilled.value = false
                return
            }
        }
        _allFieldsFilled.value = true
    }
}