package com.zeedeo.adapter.recycler.registration

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.adapter.recycler.DateClickListener
import com.zeedeo.model.LanguageLevelModel

class LanguageLevelRecyclerAdapter(
    val languageLevels: ArrayList<LanguageLevelModel> = arrayListOf(LanguageLevelModel()),
    private val levelClickListener: DateClickListener
) : RecyclerView.Adapter<LanguageLevelRecyclerAdapter.ViewHolder>() {

    private val _allFieldsFilled = MutableLiveData(false)
    val allFieldsFilled: LiveData<Boolean>
        get() = _allFieldsFilled

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_language_recycler, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return languageLevels.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(position)
    }

    fun setLevel(position: Int, level: String) {
        languageLevels[position].level = level
        notifyItemChanged(position)
        checkAllFieldsFilled()
    }

    fun addLanguage() {
        languageLevels.add(LanguageLevelModel())
        notifyItemRangeChanged(languageLevels.size - 1, languageLevels.size)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val languageEditText =
            itemView.findViewById<AppCompatEditText>(R.id.languageEditText)
        private val languageLevelTextView =
            itemView.findViewById<AppCompatTextView>(R.id.languageLevelTextView)

        fun bind(position: Int) {
            languageEditText.setText(languageLevels[position].language)
            languageLevelTextView.text = languageLevels[position].level
            languageLevelTextView.setOnClickListener {
                languageEditText.clearFocus()
                levelClickListener.onClick(position)
            }
            initTextWatchers(position)
        }

        private fun initTextWatchers(position: Int) {
            languageEditText.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    languageLevels[position].language = s.toString()
                    checkAllFieldsFilled()
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            })
            languageEditText.setOnEditorActionListener { view, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    view.clearFocus()
                    languageLevelTextView.callOnClick()
                }
                false
            }
        }
    }

    fun checkAllFieldsFilled() {
        for (item in languageLevels) {
            if (item.language.isEmpty()) {
                _allFieldsFilled.value = false
                return
            }
        }
        _allFieldsFilled.value = true
    }
}