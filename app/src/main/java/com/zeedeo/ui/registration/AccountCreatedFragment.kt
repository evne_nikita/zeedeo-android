package com.zeedeo.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.viewpager2.widget.ViewPager2
import com.zeedeo.R
import com.zeedeo.adapter.viewpager.RegistrationViewPagerAdapter
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_account_created.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import kotlin.math.roundToInt


class AccountCreatedFragment : Fragment(), KodeinAware {
    override val kodein by kodein()
    lateinit var viewModel: RegistrationViewModel
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_account_created, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
    }

    override fun onStart() {
        super.onStart()
        viewModel.allFieldsFilled.observe(viewLifecycleOwner, Observer {
            nextButton.isEnabled = it
        })
    }

    override fun onResume() {
        super.onResume()
        initViewPager()
        initClickListener()
        changeInputMode()
    }

    private fun initViewPager() {
        if (registrationViewPager.adapter == null) {
            registrationViewPager.adapter =
                RegistrationViewPagerAdapter(parentFragmentManager, lifecycle)
        }
        registrationViewPager.registerOnPageChangeCallback(object :
            ViewPager2.OnPageChangeCallback() {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                CommonUtils.hideKeyboard(registrationViewPager.context, accountCreatedFragment)
                registrationProgressBar.progress =
                    ((position + 1) / (registrationViewPager.adapter as RegistrationViewPagerAdapter).itemCount.toFloat() * 100).roundToInt()
            }
        })
        registrationViewPager.isUserInputEnabled = false
    }

    private fun changeInputMode() {
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
    }

    private fun initClickListener() {
        nextButton.setOnClickListener {
            if (registrationViewPager.currentItem + 1 == registrationViewPager.adapter?.itemCount) {
                firebaseUtils.signUp(
                    viewModel.newUser
                )
                CommonUtils.createToast(
                    getString(R.string.registrationCompletedPopupTitle),
                    getString(R.string.registrationCompletedPopupSubTitle),
                    requireContext(),
                    accountCreatedFragment,
                    skipButton.height
                )
            } else {
                registrationViewPager.currentItem++
            }
        }
        skipButton.setOnClickListener {
            firebaseUtils.signUp(viewModel.newUser)
            CommonUtils.createToast(
                getString(R.string.registrationSkippedPopupTitle),
                getString(R.string.registrationSkippedPopupSubTitle),
                requireContext(),
                accountCreatedFragment,
                skipButton.height
            )
        }
    }
}