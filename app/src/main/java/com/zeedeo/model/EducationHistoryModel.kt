package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class EducationHistoryModel(
    @SerializedName("certificateName")
    var certificateName: String = "",
    @SerializedName("educationLocation")
    var educationLocation: String = "",
    @SerializedName("educationDates")
    var dates: String = "",
    @Transient
    var imageUrl: String = ""
) : Serializable