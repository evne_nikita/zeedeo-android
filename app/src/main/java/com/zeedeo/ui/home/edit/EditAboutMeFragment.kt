package com.zeedeo.ui.home.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.zeedeo.R
import com.zeedeo.ui.home.edit.viewmodel.EditAboutMeViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditAboutMeViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_about_me.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class EditAboutMeFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel
    private val viewModelFactory: EditAboutMeViewModelFactory by instance<EditAboutMeViewModelFactory>()
    private lateinit var viewModel: EditAboutMeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_about_me, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        viewModel = viewModelFactory.create(EditAboutMeViewModel::class.java)
        viewModel.setAboutMe(firebaseUtils.currentUserLiveData.value!!.about)
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initTextWatchers()
        aboutMeEditEditText.setText(viewModel.aboutMeLiveData.value)
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
    }

    private fun initTextWatchers() {
        aboutMeEditEditText.addTextChangedListener {
            viewModel.setAboutMe(it.toString())
        }
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.setUserAbout(it.uid, viewModel.aboutMeLiveData.value!!)
        }
    }
}