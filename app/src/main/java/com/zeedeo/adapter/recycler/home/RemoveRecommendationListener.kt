package com.zeedeo.adapter.recycler.home

interface RemoveRecommendationListener {
    fun onRemoveRecommendation(position: Int)
}