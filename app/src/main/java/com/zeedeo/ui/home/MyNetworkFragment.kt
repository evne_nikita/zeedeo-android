package com.zeedeo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayoutMediator
import com.zeedeo.R
import com.zeedeo.adapter.viewpager.MyNetworkViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_my_network.*

class MyNetworkFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_network, container, false)
    }

    override fun onResume() {
        super.onResume()
        initViewPager()
    }

    private fun initViewPager() {
        myNetworkViewPager.isUserInputEnabled = false
        myNetworkViewPager.adapter = MyNetworkViewPagerAdapter(childFragmentManager, lifecycle)
        TabLayoutMediator(
            myNetworkTabLayout,
            myNetworkViewPager,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> tab.text = "My Network"
                    1 -> tab.text = "Pending"
                    2 -> tab.text = "Events"
                }
            }).attach()
    }
}