package com.zeedeo.utils

import android.net.Uri

interface UploadImageSuccessListener {
    fun onUpload(uri: Uri)
}