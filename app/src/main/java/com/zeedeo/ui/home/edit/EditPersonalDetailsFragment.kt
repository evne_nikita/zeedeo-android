package com.zeedeo.ui.home.edit

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.zeedeo.R
import com.zeedeo.ui.dialog.DatePickerDialog
import com.zeedeo.ui.home.edit.viewmodel.EditPersonalInterestsViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditPersonalInterestsViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_personal_details.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class EditPersonalDetailsFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel
    private lateinit var viewModel: EditPersonalInterestsViewModel
    private val viewModelFactory: EditPersonalInterestsViewModelFactory by instance<EditPersonalInterestsViewModelFactory>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_personal_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        viewModel = viewModelFactory.create(EditPersonalInterestsViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        firebaseUtils.currentUserLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.setUserInfo(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initTextWatchers()
        initClickListeners()
        loadData()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.DATE_REQUEST_CODE) {
            data?.let {
                val birthDate = it.getStringExtra(Constants.DATE_KEY)
                personalDetailsBirthDateTextView.text = birthDate
                viewModel.setBirthDate(birthDate ?: "")
            }
        }
    }

    private fun initTextWatchers() {
        personalDetailsRateEditText.addTextChangedListener {
            viewModel.setRate(it.toString().toFloat())
        }
        personalDetailsRateEditText.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                personalDetailsRateEditText.setText(
                    resources.getString(
                        R.string.rateEditTextTemplate,
                        viewModel.userInfoLiveData.value?.rate.toString()
                    )
                )
                CommonUtils.hideKeyboard(requireContext(), personalDetailsFragment)
            }
            false
        }

        personalDetailsFirstNameEditText.addTextChangedListener {
            viewModel.setFirstName(it.toString())
        }

        personalDetailsLastNameEditText.addTextChangedListener {
            viewModel.setLastName(it.toString())
        }

        personalDetailsEmailEditText.addTextChangedListener {
            viewModel.setEmail(it.toString())
        }

        personalDetailsLocationEditText.addTextChangedListener {
            viewModel.setLocation(it.toString())
        }

        personalDetailsJobTitleEditText.addTextChangedListener {
            viewModel.setTitle(it.toString())
        }

        personalDetailsNationalityEditText.addTextChangedListener {
            viewModel.setNationality(it.toString())
        }

        editDetailsEligibilityAutocomplete.addTextChangedListener {
            viewModel.setEligibility(it.toString())
        }

        personalDetailsPhoneNumberEditText.addTextChangedListener {
            viewModel.setPhoneNumber(it.toString())
        }

        personalDetailsPersonalSiteEditText.addTextChangedListener {
            viewModel.setSite(it.toString())
        }

        personalDetailsLinksEditText.addTextChangedListener {
            viewModel.setLinks(it.toString())
        }

        personalDetailsTwitterEditText.addTextChangedListener {
            viewModel.setTwitter(it.toString())
        }
    }

    private fun loadData() {
        firebaseUtils.currentUserLiveData.value?.let {
            personalDetailsFirstNameEditText.setText(it.firstName)
            personalDetailsLastNameEditText.setText(it.lastName)
            personalDetailsBirthDateTextView.text = it.birthDate
            personalDetailsNationalityEditText.setText(it.nationality)
            personalDetailsJobTitleEditText.setText(it.title)
            editDetailsEligibilityAutocomplete.setText(it.eligibility)
            personalDetailsEmailEditText.setText(it.email)
            personalDetailsPhoneNumberEditText.setText(it.phoneNumber)
            personalDetailsLocationEditText.setText(it.location)
            personalDetailsLinksEditText.setText(it.links)
            personalDetailsPersonalSiteEditText.setText(it.personalSite)
            personalDetailsTwitterEditText.setText(it.twitter)
            personalDetailsRateEditText.setText(it.rate.toString())
        }
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            firebaseUtils.uploadUserDetails(it.uid, viewModel.userInfoLiveData.value!!)
        }
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        personalDetailsSaveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
        personalDetailsBirthDateTextView.setOnClickListener {
            showDatePickerDialog()
        }
    }

    private fun showDatePickerDialog() {
        val dialog = DatePickerDialog(false)
        dialog.setTargetFragment(this, Constants.DATE_REQUEST_CODE)
        dialog.show(requireFragmentManager(), "datePickerDialog")
    }
}