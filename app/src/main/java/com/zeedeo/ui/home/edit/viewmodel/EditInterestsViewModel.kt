package com.zeedeo.ui.home.edit.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class EditInterestsViewModel : ViewModel() {
    private val _personalInterestsLiveData = MutableLiveData("")
    val personalInterestsLiveData: LiveData<String>
        get() = _personalInterestsLiveData

    fun setPersonalInterests(interests: String) {
        _personalInterestsLiveData.value = interests
    }
}