package com.zeedeo.adapter.recycler.home

interface RemoveLanguageListener {
    fun onRemoveLanguage(position: Int)
}