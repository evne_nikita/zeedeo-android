package com.zeedeo.ui.main

import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.zeedeo.R
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import com.zeedeo.utils.PreferencesStorage
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class MainActivity : FragmentActivity(), KodeinAware {
    override val kodein by kodein()
    private val storage: PreferencesStorage by instance<PreferencesStorage>()

    private val mainActivityViewModelFactory: MainActivityViewModelFactory by instance<MainActivityViewModelFactory>()
    lateinit var sharedViewModel: MainActivityViewModel
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        sharedViewModel = mainActivityViewModelFactory.create(MainActivityViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        firebaseUtils.currentUserLiveData.observe(this@MainActivity, Observer {
            sharedViewModel.setEducation(it.educationHistory)
            sharedViewModel.setWorkingExperience(it.workingExperience)
            sharedViewModel.setLanguages(it.languages)
            sharedViewModel.setSkills(it.skills)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        firebaseUtils.signOut()
    }

    private fun getCurrentToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    return@OnCompleteListener
                }
                val token = task.result?.token
                Toast.makeText(baseContext, "token received", Toast.LENGTH_SHORT).show()
                token?.let {
                    storage.save(Constants.TOKEN_KEY, token)
                }
            })
    }
}