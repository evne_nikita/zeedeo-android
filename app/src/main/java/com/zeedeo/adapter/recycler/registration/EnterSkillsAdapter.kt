package com.zeedeo.adapter.recycler.registration

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatImageButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.zeedeo.R
import com.zeedeo.model.SkillModel

class EnterSkillsAdapter(
    private val skills: ArrayList<SkillModel> = ArrayList(),
    private val removeSkillListener: RemoveSkillListener
) :
    RecyclerView.Adapter<EnterSkillsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_skill, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return skills.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }

    fun updateData(newList: ArrayList<SkillModel>) {
        skills.clear()
        skills.addAll(newList)
        notifyDataSetChanged()
    }

    fun removeSkill(position: Int) {
        skills.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeRemoved(0, skills.size)
    }

    fun addSkill(skill: String) {
        skills.add(SkillModel(skill))
        notifyItemInserted(skills.size - 1)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val skillTitleTextView =
            itemView.findViewById<AppCompatTextView>(R.id.skillTextView)
        private val removeSkillButton =
            itemView.findViewById<AppCompatImageButton>(R.id.removeSkillButton)

        fun bind() {
            skillTitleTextView.text = skills[adapterPosition].title

            removeSkillButton.setOnClickListener {
                removeSkillListener.onSkillRemoved(adapterPosition)
            }
        }
    }
}