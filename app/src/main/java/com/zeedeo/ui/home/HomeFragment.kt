package com.zeedeo.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.adapter.viewpager.HomeViewPagerAdapter
import com.zeedeo.model.UserModel
import com.zeedeo.ui.dialog.AddItemDialog
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import com.zeedeo.utils.setDrawableColor
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class HomeFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
    }

    override fun onStart() {
        super.onStart()
        firebaseUtils.currentUserLiveData.observe(viewLifecycleOwner, Observer {
            getProfileImage(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initViewPager()
        initClickListeners()
    }

    private fun getProfileImage(user: UserModel) {
        val profileImageReference = firebaseUtils.usersStorageReference.child(user.uid)
            .child(Constants.STORAGE_PROFILE_IMAGE_PHOTO_REFERENCE)
            .child(Constants.STORAGE_PROFILE_IMAGE_FILE_NAME)
        profileImageReference.downloadUrl.addOnSuccessListener { uri ->
            val imageUrl: String = uri.toString()
            Glide.with(requireContext()).load(imageUrl).circleCrop()
                .into(myProfileImageView)
        }
    }

    private fun initViewPager() {
        if (homeFragmentViewPager.adapter == null) {
            homeFragmentViewPager.adapter = HomeViewPagerAdapter(childFragmentManager, lifecycle)
        }
        homeFragmentViewPager.isUserInputEnabled = false
    }

    private fun initClickListeners() {
        addItemButton.setOnClickListener {
            showAddItemDialog()
        }
        myProfileImageView.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_myProfileFragment)
        }
        footerHomeIcon.setOnClickListener {
            (it as AppCompatTextView).setDrawableColor(R.color.colorPrimary)
            homeFragmentViewPager.setCurrentItem(Constants.HOME_FRAGMENT_INDEX, false)
            footerMessagesIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerNetworkIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerMyJobsIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
        }
        footerMessagesIcon.setOnClickListener {
            (it as AppCompatTextView).setDrawableColor(R.color.colorPrimary)
            homeFragmentViewPager.setCurrentItem(Constants.MESSAGES_FRAGMENT_INDEX, false)
            footerHomeIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerNetworkIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerMyJobsIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
        }
        footerMyJobsIcon.setOnClickListener {
            (it as AppCompatTextView).setDrawableColor(R.color.colorPrimary)
            homeFragmentViewPager.setCurrentItem(Constants.JOBS_FRAGMENT_INDEX, false)
            footerMessagesIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerNetworkIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerHomeIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
        }
        footerNetworkIcon.setOnClickListener {
            (it as AppCompatTextView).setDrawableColor(R.color.colorPrimary)
            homeFragmentViewPager.setCurrentItem(Constants.NETWORK_FRAGMENT_INDEX, false)
            footerHomeIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerMessagesIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
            footerMyJobsIcon.setDrawableColor(
                R.color.hintTextColor,
                R.color.profileJobDetailsTextColor
            )
        }
    }

    private fun showAddItemDialog() {
        val dialog = AddItemDialog()
        dialog.setTargetFragment(this, Constants.ADD_ITEM_REQUEST_CODE)
        dialog.show(requireFragmentManager(), "addItemDialog")
    }
}