package com.zeedeo.ui.home.edit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.home.EditQuotesRecyclerAdapter
import com.zeedeo.adapter.recycler.home.RemoveQuoteClickListener
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_quotes.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.IOException


class EditQuotesFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private lateinit var sharedViewModel: MainActivityViewModel
    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_quotes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
    }

    override fun onResume() {
        super.onResume()
        initRecycler()
        initClickListeners()
        sharedViewModel.quotesLiveData.observe(viewLifecycleOwner, Observer {
            (editQuotesRecyclerView.adapter as EditQuotesRecyclerAdapter).updateData(it)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // load image from gallery
        if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    uploadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            uploadImage(CommonUtils.getUriFromBitmap(photo as Bitmap, requireContext()))

            // pick image dialog result
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, getString(R.string.selectImageDialogTitle)),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            findNavController().popBackStack()
        }
        editQuotesUploadFileButton.setOnClickListener {
            val dialogFragment = PickImageDialog()
            dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
            fragmentManager?.let { dialogFragment.show(it, "Upload Image Dialog") }
        }
    }

    private fun initRecycler() {
        editQuotesRecyclerView.layoutManager =
            GridLayoutManager(context, 2, GridLayoutManager.HORIZONTAL, false)
        editQuotesRecyclerView.adapter =
            EditQuotesRecyclerAdapter(removeClickListener = object : RemoveQuoteClickListener {
                override fun onRemoveClick(position: Int) {
                    (editQuotesRecyclerView.adapter as EditQuotesRecyclerAdapter).removeQuote(
                        position
                    )
                    sharedViewModel.removeQuote(position)
                }
            })
    }

    private fun uploadImage(uri: Uri) {
        sharedViewModel.addQuote(uri)
        val userId = firebaseUtils.currentUserLiveData.value!!.uid
        val reference = Constants.STORAGE_QUOTES_PHOTO_REFERENCE
        firebaseUtils.uploadImage(
            uri,
            reference,
            "${userId}_quote${sharedViewModel.quotesLiveData.value!!.size}"
        )
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }
}