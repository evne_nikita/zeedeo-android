package com.zeedeo.ui.registration

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.MarginItemDecoration
import com.zeedeo.adapter.recycler.registration.EnterSkillsAdapter
import com.zeedeo.adapter.recycler.registration.RemoveSkillListener
import com.zeedeo.ui.auth.AuthActivity
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.fragment_enter_skills.*

class EnterSkillsFragment : Fragment() {

    private lateinit var viewModel: RegistrationViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_enter_skills, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = (activity as AuthActivity).sharedRegistrationViewModel
    }

    override fun onStart() {
        super.onStart()
        viewModel.skillsNumberLiveData.observe(viewLifecycleOwner, Observer {
            viewModel.setAllFieldsFilled(it > 0)
        })
    }

    override fun onResume() {
        super.onResume()
        initRecycler()
        initTextWatchers()
        viewModel.setAllFieldsFilled(false)
    }

    private fun initTextWatchers() {
        enterSkillsEditText.setOnEditorActionListener { view, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_NEXT) {
                (enterSkillsRecyclerView.adapter as EnterSkillsAdapter).addSkill(view.text.toString())
                viewModel.addSkill(view.text.toString())
                view.text = ""
                view.requestFocus()
            }
            true
        }
    }

    private fun initRecycler() {
        val chipsManager = ChipsLayoutManager.newBuilder(context)
            .setScrollingEnabled(true)
            .setOrientation(ChipsLayoutManager.HORIZONTAL)
            .build()
        enterSkillsRecyclerView.layoutManager = chipsManager
        enterSkillsRecyclerView.adapter = EnterSkillsAdapter(removeSkillListener = object: RemoveSkillListener {
            override fun onSkillRemoved(position: Int) {
                (enterSkillsRecyclerView.adapter as EnterSkillsAdapter).removeSkill(position)
                viewModel.removeSkill(position)
            }
        })
        enterSkillsRecyclerView.addItemDecoration(
            MarginItemDecoration(
                Constants.SKILL_ITEM_MARGIN
            ), Constants.SKILL_ITEM_MARGIN)
    }
}