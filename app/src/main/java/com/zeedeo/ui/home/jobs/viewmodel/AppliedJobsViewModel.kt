package com.zeedeo.ui.home.jobs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.JobModel

class AppliedJobsViewModel: ViewModel() {
    private val _appliedJobsLiveData = MutableLiveData(ArrayList<JobModel>())
    val appliedJobsLiveData: LiveData<ArrayList<JobModel>>
    get() = _appliedJobsLiveData

    fun setAppliedJobs(jobs: ArrayList<JobModel>){
        _appliedJobsLiveData.value = jobs
    }

    fun removeJob(position: Int) {
        _appliedJobsLiveData.value?.removeAt(position)
    }
}