package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class JobModel(
    @SerializedName("companyId")
    var companyId: String = "",
    @SerializedName("companyName")
    var companyName: String = "",
    @SerializedName("companyImage")
    var companyImage: String = "",
    @SerializedName("companyLocation")
    var companyLocation: String = "",
    @SerializedName("createdAt")
    var createdAt: String = "",
    @SerializedName("detailsDescription")
    var detailsDescription: String = "",
    @SerializedName("detailsVideoLink")
    var detailsVideoLink: String = "",
    @SerializedName("eligibilityToWork")
    var eligibilityToWork: String = "",
    @SerializedName("id")
    var id: String = "",
    @SerializedName("jobExpirationDate")
    var jobExprirationDate: String = "",
    @SerializedName("languages")
    var languages: ArrayList<LanguageLevelModel> = ArrayList(),
    @SerializedName("location")
    var location: String = "",
    @SerializedName("position")
    var position: String = "",
    @SerializedName("questions")
    var questions: ArrayList<QuestionModel> = ArrayList(),
    @SerializedName("salaryCurrency")
    var currency: String = "",
    @SerializedName("salaryMax")
    var salaryMax: Int = 0,
    @SerializedName("salaryMin")
    var salaryMin: Int = 0,
    @SerializedName("salaryType")
    var salaryType: String = "",
    @SerializedName("skills")
    var skills: ArrayList<SkillModel> = ArrayList(),
    @SerializedName("title")
    var title: String = ""
): Serializable