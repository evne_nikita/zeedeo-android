package com.zeedeo.ui.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.zeedeo.R
import kotlinx.android.synthetic.main.dialog_add_item.*

class AddItemDialog: DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_add_item, container, false)
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
    }

    private fun initClickListeners() {
        addContactOption.setOnClickListener {
            dismiss()
        }
        createEventOption.setOnClickListener {
            dismiss()
        }
    }
}