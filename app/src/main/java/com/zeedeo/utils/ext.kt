package com.zeedeo.utils

import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.util.Patterns
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat

fun AppCompatTextView.setDrawableColor(drawableColor: Int, textColor: Int = drawableColor) {
    for (drawable in this.compoundDrawables) {
        if (drawable != null) {
            drawable.colorFilter = PorterDuffColorFilter(
                ContextCompat.getColor(
                    this.context,
                    drawableColor
                ), PorterDuff.Mode.SRC_IN
            )
        }
    }
    this.setTextColor(ContextCompat.getColor(this.context, drawableColor))
}

fun CharSequence.isValidEmail() = !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()
