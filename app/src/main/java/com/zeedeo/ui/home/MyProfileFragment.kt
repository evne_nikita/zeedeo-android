package com.zeedeo.ui.home

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.adapter.recycler.home.EducationHistoryRecyclerAdapter
import com.zeedeo.adapter.recycler.home.LanguageRecyclerAdapter
import com.zeedeo.adapter.recycler.home.QuotesRecyclerAdapter
import com.zeedeo.adapter.recycler.home.WorkingExperienceRecyclerAdapter
import com.zeedeo.model.UserModel
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import com.zeedeo.utils.UploadImageSuccessListener
import kotlinx.android.synthetic.main.fragment_my_profile.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.IOException

class MyProfileFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    private lateinit var sharedViewModel: MainActivityViewModel

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_my_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        initRecyclers()
    }

    override fun onStart() {
        super.onStart()
        firebaseUtils.currentUserLiveData.observe(viewLifecycleOwner, Observer {
            myProfileUserNameTextView.text = if (it == null) {
                "Unauthorized"
            } else {
                getProfileImage(it)
                getQuotes()
                myProfileJobTitleTextView.text = it.title
                myProfileLocationTextView.text = it.location
                myProfileAboutMeTextView.text = it.about
                myProfilePersonalInterestsTextView.text = it.personalInterests
                getString(R.string.firstLastNamePlaceHolder, it.firstName, it.lastName)
            }
        })
        sharedViewModel.quotesLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileQuotesRecyclerView.adapter as QuotesRecyclerAdapter).updateData(it)
        })

        sharedViewModel.workingExperienceLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileWorkingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).updateData(
                it
            )
            getWorkingExperienceImages()
        })
        sharedViewModel.workingExperienceImagesLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileWorkingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).loadCompanyLogos(
                it
            )
        })
        sharedViewModel.educationLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileEducationRecyclerView.adapter as EducationHistoryRecyclerAdapter).updateData(
                it
            )
            getEducationImages()
        })
        sharedViewModel.educationImagesLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileEducationRecyclerView.adapter as EducationHistoryRecyclerAdapter).loadCertificates(
                it
            )
        })
        sharedViewModel.skillsLiveData.observe(viewLifecycleOwner, Observer {
            var skills = ""
            for (i in 0 until it.size - 1) {
                skills += "${it[i].title}, "
            }
            if (it.isNotEmpty()) {
                skills += "${it[it.size - 1].title}."
            }
            myProfileSkillsTextView.text = skills
        })
        sharedViewModel.languagesLiveData.observe(viewLifecycleOwner, Observer {
            (myProfileLanguagesRecyclerView.adapter as LanguageRecyclerAdapter).updateData(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
    }

    override fun onStop() {
        super.onStop()
        firebaseUtils.currentUserLiveData.removeObservers(viewLifecycleOwner)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    uploadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            photo?.let {
                uploadImage(CommonUtils.getUriFromBitmap(photo as Bitmap, requireContext()))
            }
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun initClickListeners() {
        myProfileBackButton.setOnClickListener {
            findNavController().popBackStack()
        }
        myProfileShareImageButton.setOnClickListener {
            val sendIntent = Intent()
            sendIntent.action = Intent.ACTION_SEND
            sendIntent.putExtra(
                Intent.EXTRA_TEXT,
                getString(R.string.debugShareIntentMessage)
            )
            sendIntent.type = "text/plain"
            startActivity(sendIntent)
        }
        publicViewImageButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_userFragment)
        }
        myProfileEditAboutMeButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editAboutMeFragment)
        }
        myProfileEditNameButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_personalDetailsFragment)
        }
        myProfileEditQuotesButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editQuotesFragment)
        }
        myProfileEditWorkingExperienceButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editWorkingExperienceFragment)
        }
        editSkillsButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editSkillsFragment)
        }
        myProfileEditLanguagesButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editLanguagesFragment)
        }
        myProfileEditEducationButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editEducationFragment)
        }
        myProfileEditPersonalInterestsButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editPersonalInterestsFragment)
        }
        myProfileEditRecommendationsButton.setOnClickListener {
            findNavController().navigate(R.id.action_homeFragment_to_editRecommendationsFragment)
        }
        editProfileImageButton.setOnClickListener {
            showPickImageDialog()
        }
    }

    private fun initRecyclers() {
        myProfileQuotesRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        myProfileQuotesRecyclerView.adapter = QuotesRecyclerAdapter()

        myProfileWorkingExperienceRecyclerView.layoutManager = LinearLayoutManager(context)
        myProfileWorkingExperienceRecyclerView.adapter = WorkingExperienceRecyclerAdapter()

        myProfileEducationRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        myProfileEducationRecyclerView.adapter = EducationHistoryRecyclerAdapter()

        myProfileLanguagesRecyclerView.layoutManager = LinearLayoutManager(context)
        myProfileLanguagesRecyclerView.adapter = LanguageRecyclerAdapter()

        myProfileRecommendationsRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun getQuotes() {
        val reference =
            firebaseUtils.usersStorageReference.child(firebaseUtils.currentUserLiveData.value!!.uid)
                .child(Constants.STORAGE_QUOTES_PHOTO_REFERENCE)
        reference.listAll().addOnSuccessListener {
            it?.let { result ->
                for (item in result.items) {
                    item.downloadUrl.addOnSuccessListener { url ->
                        url?.let { uri ->
                            sharedViewModel.addQuote(uri)
                        }
                    }
                }
            }
        }
    }

    private fun getWorkingExperienceImages() {
        val reference =
            firebaseUtils.usersStorageReference.child(firebaseUtils.currentUserLiveData.value!!.uid)
                .child(Constants.STORAGE_WORKING_EXPERIENCE_PHOTO_REFERENCE)
        reference.listAll().addOnSuccessListener {
            it?.let { result ->
                val images = ArrayList<Uri>()
                for (item in result.items) {
                    item.downloadUrl.addOnSuccessListener { url ->
                        images.add(url)
                        if (images.size == result.items.size) {
                            sharedViewModel.setWorkingExperienceImages(images)
                        }
                    }
                }
            }
        }
    }

    private fun getEducationImages() {
        val reference =
            firebaseUtils.usersStorageReference.child(firebaseUtils.currentUserLiveData.value!!.uid)
                .child(Constants.STORAGE_EDUCATION_PHOTO_REFERENCE)
        reference.listAll().addOnSuccessListener {
            it?.let { result ->
                val images = ArrayList<Uri>()
                for (item in result.items) {
                    item.downloadUrl.addOnSuccessListener { url ->
                        images.add(url)
                        if (images.size == result.items.size) {
                            sharedViewModel.setEducationImages(images)
                        }
                    }
                }
            }
        }
    }

    private fun getProfileImage(user: UserModel) {
        val profileImageReference = firebaseUtils.usersStorageReference.child(user.uid)
            .child(Constants.STORAGE_PROFILE_IMAGE_PHOTO_REFERENCE)
            .child(Constants.STORAGE_PROFILE_IMAGE_FILE_NAME)
        profileImageReference.downloadUrl.addOnSuccessListener { uri ->
            val imageUrl: String = uri.toString()
            Glide.with(requireContext()).load(imageUrl).into(videoPreviewImageView)
            Glide.with(requireContext()).load(imageUrl).circleCrop()
                .into(myProfileAvatarImageView)
        }
    }

    private fun showPickImageDialog() {
        val dialogFragment = PickImageDialog()
        dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
        dialogFragment.show(requireFragmentManager(), "Load image Dialog")
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }

    private fun uploadImage(imageUri: Uri) {
        firebaseUtils.uploadImage(
            Uri.parse(imageUri.toString()),
            Constants.STORAGE_PROFILE_IMAGE_PHOTO_REFERENCE,
            Constants.STORAGE_PROFILE_IMAGE_FILE_NAME,
            object : UploadImageSuccessListener {
                override fun onUpload(uri: Uri) {
                    Glide.with(requireContext()).load(uri).circleCrop()
                        .into(myProfileAvatarImageView)
                    Glide.with(requireContext()).load(uri).into(videoPreviewImageView)
                }
            }
        )
    }
}