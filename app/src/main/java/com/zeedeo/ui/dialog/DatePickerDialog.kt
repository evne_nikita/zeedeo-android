package com.zeedeo.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.zeedeo.R
import com.zeedeo.utils.Constants
import com.zeedeo.utils.TimeUtils
import kotlinx.android.synthetic.main.dialog_date_picker.*

class DatePickerDialog(private val hasEndDate: Boolean) : DialogFragment() {
    private var startDate = ""
    private var onEndDatePicker = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_date_picker, container, false)
    }

    override fun onResume() {
        super.onResume()
        initDatePickerListeners()
    }

    private fun initDatePickerListeners() {
        cancelTextView.setOnClickListener {
            dismiss()
        }
        if (!hasEndDate) {
            datePickerDialogTitle.text = getString(R.string.birthDateDialogTitle)
            okTextView.setOnClickListener {
                sendData(TimeUtils.formatDate(startDatePicker))
                dismiss()
            }

        } else {
            okTextView.setOnClickListener {
                if (onEndDatePicker) {
                    sendData(
                        resources.getString(
                            R.string.durationText,
                            startDate,
                            TimeUtils.formatDate(endDatePicker)
                        )
                    )
                    dismiss()
                } else {
                    startDatePicker.visibility = View.INVISIBLE
                    startDate = TimeUtils.formatDate(startDatePicker)
                    endDatePicker.visibility = View.VISIBLE
                    datePickerDialogTitle.text = resources.getString(R.string.endDatePickTitle)
                    onEndDatePicker = true
                }
            }
        }
    }

    private fun sendData(value: String) {
        val intent = Intent()
        intent.putExtra(Constants.DATE_KEY, value)
        targetFragment!!.onActivityResult(
            targetRequestCode, Constants.DATE_REQUEST_CODE, intent
        )
    }
}