package com.zeedeo.ui.home.edit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.home.EditRecommendationsRecyclerAdapter
import com.zeedeo.adapter.recycler.home.RemoveRecommendationListener
import kotlinx.android.synthetic.main.fragment_edit_education.backButton
import kotlinx.android.synthetic.main.fragment_edit_education.saveButton
import kotlinx.android.synthetic.main.fragment_edit_recommendations.*

class EditRecommendationsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_recommendations, container, false)
    }

    override fun onResume() {
        super.onResume()
        initRecycler()
        initClickListeners()
    }

    private fun initRecycler() {
        editRecommendationsRecyclerView.layoutManager = LinearLayoutManager(context)
        editRecommendationsRecyclerView.adapter =
            EditRecommendationsRecyclerAdapter(removeRecommendationListener = object :
                RemoveRecommendationListener {
                override fun onRemoveRecommendation(position: Int) {
                    (editRecommendationsRecyclerView.adapter as EditRecommendationsRecyclerAdapter).removeRecommendation(
                        position
                    )
                }
            })
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            findNavController().popBackStack()
        }
        askForRecommendationsTextView.setOnClickListener {
            (editRecommendationsRecyclerView.adapter as EditRecommendationsRecyclerAdapter).addRecommendation()
        }
    }
}