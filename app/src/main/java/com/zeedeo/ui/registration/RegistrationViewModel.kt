package com.zeedeo.ui.registration

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.SkillModel
import com.zeedeo.model.UserModel

class RegistrationViewModel : ViewModel() {

    var newUser = UserModel()

    private val _allFieldsFilledLiveData = MutableLiveData(false)
    val allFieldsFilled: LiveData<Boolean>
        get() = _allFieldsFilledLiveData

    fun setAllFieldsFilled(filled: Boolean) {
        _allFieldsFilledLiveData.value = filled
    }

    private val _skillsNumberLiveData = MutableLiveData(0)
    val skillsNumberLiveData: LiveData<Int>
        get() = _skillsNumberLiveData

    fun addSkill(skill: String) {
        newUser.skills.add(SkillModel(skill))
        _skillsNumberLiveData.value = _skillsNumberLiveData.value?.plus(1)
    }

    fun removeSkill(position: Int) {
        newUser.skills.removeAt(position)
        _skillsNumberLiveData.value = _skillsNumberLiveData.value?.minus(1)
    }
}