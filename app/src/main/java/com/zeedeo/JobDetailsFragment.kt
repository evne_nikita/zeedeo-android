package com.zeedeo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.bumptech.glide.Glide
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.fragment_job_details.*

class JobDetailsFragment : Fragment() {

    private lateinit var sharedViewModel: MainActivityViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        sharedElementEnterTransition = TransitionInflater.from(context)
            .inflateTransition(android.R.transition.move)
        sharedElementReturnTransition = TransitionInflater.from(context)
            .inflateTransition(android.R.transition.move)
        return inflater.inflate(R.layout.fragment_job_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
    }

    override fun onResume() {
        super.onResume()
        loadImages()
        displayData()
        initClickListeners()
    }

    private fun loadImages() {
        Glide.with(requireContext()).load(Constants.DEBUG_JOB_PREVIEW_URL)
            .centerCrop()
            .into(jobDetailsPreviewImageView)
        Glide.with(requireContext()).load(Constants.DEBUG_COMPANY_LOGO_URL)
            .circleCrop()
            .into(jobDetailsCompanyLogoImageView)
    }

    private fun displayData() {
        jobDetailsCompanyNameTextView.text = sharedViewModel.selectedJobModel.companyName
        jobDetailsCompanyDescriptionTextView.text = sharedViewModel.selectedJobModel.detailsDescription
        jobDetailsCompanyLocationTextView.text = sharedViewModel.selectedJobModel.companyLocation
        jobDetailsDescriptionTextView.text = sharedViewModel.selectedJobModel.detailsDescription
        jobDetailsSalaryTextView.text = getString(
            R.string.salaryPlaceHolder,
            sharedViewModel.selectedJobModel.salaryMin,
            sharedViewModel.selectedJobModel.salaryMax
        )
        jobDetailsPositionTextView.text = sharedViewModel.selectedJobModel.title
        jobDetailsLocationTitle.text = sharedViewModel.selectedJobModel.location
    }

    private fun initClickListeners() {
        jobDetailsShareIcon.setOnClickListener {
            CommonUtils.createShareIntent(requireContext())
        }
        jobDetailsDismissTextView.setOnClickListener {
            sharedViewModel.dismissedJobs.add(sharedViewModel.selectedJobModel)
            findNavController().popBackStack()
        }
        jobDetailsApplyButton.setOnClickListener {
            sharedViewModel.appliedJobs.add(sharedViewModel.selectedJobModel)
            findNavController().popBackStack()
        }
        jobDetailsBackButton.setOnClickListener {
            findNavController().popBackStack()
        }
    }
}