package com.zeedeo.adapter.viewpager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.zeedeo.ui.home.network.MyContactsFragment
import com.zeedeo.ui.home.network.NetworkEventsFragment
import com.zeedeo.ui.home.network.NetworkPendingFragment

class MyNetworkViewPagerAdapter(fragmentManager: FragmentManager, lifecycle: Lifecycle) :
    FragmentStateAdapter(fragmentManager, lifecycle) {
    private val itemCount = 3

    override fun getItemCount(): Int {
        return itemCount
    }

    override fun createFragment(position: Int): Fragment {
        var fragment: Fragment? = null
        when(position) {
            0 -> fragment = MyContactsFragment()
            1 -> fragment = NetworkPendingFragment()
            2 -> fragment = NetworkEventsFragment()
        }
        return fragment!!
    }
}