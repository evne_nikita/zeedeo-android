package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class UserModel(
    @Transient
    var uid: String = "",
    @SerializedName("email")
    var email: String = "",
    @SerializedName("password")
    var password: String = "",
    @SerializedName("firstName")
    var firstName: String = "",
    @SerializedName("lastName")
    var lastName: String = "",
    @SerializedName("profileImage")
    var profileImage: String = "",
    @SerializedName("location")
    var location: String = "",
    @SerializedName("title")
    var title: String = "",
    @SerializedName("about")
    var about: String = "",
    @SerializedName("nationality")
    var nationality: String = "",
    @SerializedName("birthDate")
    var birthDate: String = "",
    @SerializedName("eligibility")
    var eligibility: String = "",
    @SerializedName("phoneNumber")
    var phoneNumber: String = "",
    @SerializedName("links")
    var links: String = "",
    @SerializedName("personalSite")
    var personalSite: String = "",
    @SerializedName("twitter")
    var twitter: String = "",
    @SerializedName("rate")
    var rate: Float = 0.0f,
    @SerializedName("personalInterests")
    var personalInterests: String = "",
    @Transient
    var skills: ArrayList<SkillModel> = ArrayList(),
    @Transient
    var workingExperience: ArrayList<WorkingExperienceModel> = ArrayList(),
    @Transient
    var educationHistory: ArrayList<EducationHistoryModel> = ArrayList(),
    @Transient
    var languages: ArrayList<LanguageLevelModel> = ArrayList()
) : Serializable