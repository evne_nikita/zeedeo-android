package com.zeedeo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.zeedeo.R
import com.zeedeo.adapter.recycler.home.EducationHistoryRecyclerAdapter
import com.zeedeo.adapter.recycler.home.WorkingExperienceRecyclerAdapter
import com.zeedeo.model.EducationHistoryModel
import com.zeedeo.model.SkillModel
import com.zeedeo.model.WorkingExperienceModel
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_view_user.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class ViewUserFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_view_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclers()
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
        initDebugWorkingExperience()
        initDebugEducation()
        initDebugSkills()
        initDebugImages()
    }

    private fun initDebugWorkingExperience() {
        val workingExperience = ArrayList<WorkingExperienceModel>()
        for (i in 0..5) {
            workingExperience.add(WorkingExperienceModel(imageUrl = Constants.DEBUG_COMPANY_LOGO_URL))
        }
        (viewUserWorkingExperienceRecyclerView.adapter as WorkingExperienceRecyclerAdapter).updateData(
            workingExperience
        )
    }

    private fun initDebugEducation() {
        val education = ArrayList<EducationHistoryModel>()
        for (i in 0..5) {
            education.add(EducationHistoryModel(imageUrl = Constants.DEBUG_CERTIFICATE_URL))
        }
        (viewUserEducationRecyclerView.adapter as EducationHistoryRecyclerAdapter).updateData(
            education
        )
    }

    private fun initDebugSkills() {
        var skills = ""
        val skillsList = ArrayList<SkillModel>()
        for (i in 0..5) {
            skillsList.add(SkillModel("Skill $i"))
        }


        for (i in 0 until skillsList.size) {
            skills += "${skillsList[i].title}, "
        }
        if (skillsList.isNotEmpty()) {
            skills += "${skillsList[skillsList.size - 1].title}."
        }
        viewUserSkillsTextView.text = skills
    }

    private fun initClickListeners() {
        viewUserShareImageButton.setOnClickListener {
           CommonUtils.createShareIntent(requireContext())
        }
        viewUserBackButton.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun initRecyclers() {
        viewUserWorkingExperienceRecyclerView.adapter = WorkingExperienceRecyclerAdapter()
        viewUserWorkingExperienceRecyclerView.layoutManager = LinearLayoutManager(context)
        viewUserEducationRecyclerView.adapter = EducationHistoryRecyclerAdapter()
        viewUserEducationRecyclerView.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun initDebugImages() {
        Glide.with(requireContext()).load(Constants.DEBUG_PROFILE_URL).into(videoPreviewImageView)
        Glide.with(requireContext()).load(Constants.DEBUG_PROFILE_URL).into(viewUserAvatarImageView)
    }
}