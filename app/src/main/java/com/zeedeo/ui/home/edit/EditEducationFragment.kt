package com.zeedeo.ui.home.edit

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.zeedeo.R
import com.zeedeo.adapter.recycler.AddImageListener
import com.zeedeo.adapter.recycler.home.EditEducationRecyclerAdapter
import com.zeedeo.adapter.recycler.home.RemoveEducationListener
import com.zeedeo.ui.dialog.DatePickerDialog
import com.zeedeo.ui.dialog.PickImageDialog
import com.zeedeo.ui.home.edit.viewmodel.EditEducationViewModel
import com.zeedeo.ui.home.edit.viewmodel.EditEducationViewModelFactory
import com.zeedeo.ui.main.MainActivity
import com.zeedeo.ui.main.MainActivityViewModel
import com.zeedeo.utils.CallDatePickerListener
import com.zeedeo.utils.CommonUtils
import com.zeedeo.utils.Constants
import com.zeedeo.utils.FirebaseUtils
import kotlinx.android.synthetic.main.fragment_edit_education.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.IOException


class EditEducationFragment : Fragment(), KodeinAware {
    override val kodein by kodein()

    private val firebaseUtils: FirebaseUtils by instance<FirebaseUtils>()
    private lateinit var sharedViewModel: MainActivityViewModel
    private lateinit var viewModel: EditEducationViewModel
    private val viewModelFactory: EditEducationViewModelFactory by instance<EditEducationViewModelFactory>()

    private val requiredPermissions = arrayOf(Manifest.permission.CAMERA)

    private var selectedItem = -1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_edit_education, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedViewModel = (activity as MainActivity).sharedViewModel
        viewModel = viewModelFactory.create(EditEducationViewModel::class.java)
        viewModel.setEducation(sharedViewModel.educationLiveData.value!!)
        viewModel.setImages(sharedViewModel.educationImagesLiveData.value!!)
        initRecycler()
    }

    override fun onStart() {
        super.onStart()
        viewModel.educationLiveData.observe(this@EditEducationFragment, Observer {
            (editEducationRecyclerView.adapter as EditEducationRecyclerAdapter).updateData(it)
        })
        viewModel.educationImagesLiveData.observe(this@EditEducationFragment, Observer {
            (editEducationRecyclerView.adapter as EditEducationRecyclerAdapter).loadCertificates(it)
        })
    }

    override fun onResume() {
        super.onResume()
        initClickListeners()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Constants.DATE_REQUEST_CODE) {
            data?.let {
                (editEducationRecyclerView.adapter as EditEducationRecyclerAdapter).setDates(
                    selectedItem,
                    data.getStringExtra(Constants.DATE_KEY)!!
                )
            }
            // load image from gallery
        } else if (requestCode == Constants.LOAD_FROM_GALLERY_REQUEST_CODE && resultCode == Activity.RESULT_OK && data != null && data.data != null
        ) {
            val filePath = data.data
            try {
                filePath?.let {
                    loadImage(it)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            // load image from camera
        } else if (requestCode == Constants.LOAD_FROM_CAMERA_REQUEST_CODE) {
            val photo = data?.extras?.get("data")
            photo?.let {
                loadImage(CommonUtils.getUriFromBitmap(it as Bitmap, requireContext()))
            }
        } else if (requestCode == Constants.LOAD_IMAGE_REQUEST_CODE) {
            if (data?.getIntExtra(
                    Constants.LOAD_IMAGE_TAG,
                    -1
                ) == Constants.LOAD_FROM_GALLERY_REQUEST_CODE
            ) {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    Intent.createChooser(intent, "Select Image"),
                    Constants.LOAD_FROM_GALLERY_REQUEST_CODE
                )
                // start camera intent
            } else {
                if (allPermissionsGranted()) {
                    val takePicture = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    startActivityForResult(takePicture, Constants.LOAD_FROM_CAMERA_REQUEST_CODE)
                } else {
                    ActivityCompat.requestPermissions(
                        requireActivity(),
                        requiredPermissions,
                        Constants.CAMERA_REQUEST_CODE
                    )
                }
            }
        }
    }

    private fun loadImage(uri: Uri) {
        viewModel.setCertificateImage(selectedItem, uri)
    }

    private fun initClickListeners() {
        backButton.setOnClickListener {
            findNavController().popBackStack()
        }
        saveButton.setOnClickListener {
            applyChanges()
            findNavController().popBackStack()
        }
        addMoreTextView.setOnClickListener {
            viewModel.addEducation()
        }
    }

    private fun initRecycler() {
        editEducationRecyclerView.layoutManager = LinearLayoutManager(context)
        editEducationRecyclerView.adapter =
            EditEducationRecyclerAdapter(education = sharedViewModel.educationLiveData.value!!,
                removeEducationListener = object :
                    RemoveEducationListener {
                    override fun onRemoveEducation(position: Int) {
                        viewModel.removeEducation(position)
                    }
                },
                datePickerListener = object : CallDatePickerListener {
                    override fun datePickerCalled(position: Int) {
                        selectedItem = position
                        showDateDialog()
                    }
                },
                addImageListener = object : AddImageListener {
                    override fun addImage(position: Int) {
                        selectedItem = position
                        showGetImageDialog()
                    }
                })
    }

    private fun showDateDialog() {
        val dialogFragment = DatePickerDialog(true)
        dialogFragment.setTargetFragment(this, Constants.DATE_REQUEST_CODE)
        fragmentManager?.let { dialogFragment.show(it, "Education Dialog") }
    }

    private fun applyChanges() {
        firebaseUtils.currentUserLiveData.value?.let {
            val education =
                (editEducationRecyclerView.adapter as EditEducationRecyclerAdapter).education
            firebaseUtils.uploadEducation(it.uid, education)
            firebaseUtils.updateEducationImages((editEducationRecyclerView.adapter as EditEducationRecyclerAdapter).education)
        }
    }

    private fun showGetImageDialog() {
        val dialogFragment = PickImageDialog()
        dialogFragment.setTargetFragment(this, Constants.LOAD_IMAGE_REQUEST_CODE)
        dialogFragment.show(requireFragmentManager(), "Load image Dialog")
    }

    private fun allPermissionsGranted() =
        requiredPermissions.all {
            ContextCompat.checkSelfPermission(
                requireContext(), it
            ) == PackageManager.PERMISSION_GRANTED
        }
}