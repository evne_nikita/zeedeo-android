package com.zeedeo.utils

import android.widget.DatePicker

object TimeUtils {
    fun formatDate(day: Int, month: Int, year: Int): String {
        return "$day ${getMonth(month)} $year"
    }

    fun formatDate(datePicker: DatePicker): String {
        return formatDate(datePicker.dayOfMonth, datePicker.month, datePicker.year)
    }

    private fun getMonth(month: Int): String {
        return when (month) {
            0 -> "Jan"
            1 -> "Feb"
            2 -> "Mar"
            3 -> "Apr"
            4 -> "May"
            5 -> "Jun"
            6 -> "Jul"
            7 -> "Aug"
            8 -> "Sep"
            9 -> "Oct"
            10 -> "Nov"
            11 -> "Dec"
            else -> ""
        }
    }
}