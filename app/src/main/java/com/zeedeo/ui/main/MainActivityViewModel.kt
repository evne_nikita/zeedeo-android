package com.zeedeo.ui.main

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.*

class MainActivityViewModel : ViewModel() {

    // this only a test
    var dismissedJobs = ArrayList<JobModel>()
    var appliedJobs = ArrayList<JobModel>()
    var selectedJobModel = JobModel()

    private val _quotesLiveData = MutableLiveData<ArrayList<Uri>>(ArrayList())
    val quotesLiveData: LiveData<ArrayList<Uri>>
        get() = _quotesLiveData

    private val _workingExperienceImagesLiveData = MutableLiveData<ArrayList<Uri>>(ArrayList())
    val workingExperienceImagesLiveData: LiveData<ArrayList<Uri>>
        get() = _workingExperienceImagesLiveData

    private val _educationImagesLiveData = MutableLiveData<ArrayList<Uri>>(ArrayList())
    val educationImagesLiveData: LiveData<ArrayList<Uri>>
        get() = _educationImagesLiveData

    private val _workingExperienceLiveData = MutableLiveData<ArrayList<WorkingExperienceModel>>()
    val workingExperienceLiveData: LiveData<ArrayList<WorkingExperienceModel>>
        get() = _workingExperienceLiveData

    fun setWorkingExperience(workingExperience: ArrayList<WorkingExperienceModel>) {
        _workingExperienceLiveData.value = ArrayList(workingExperience)
    }

    private val _educationLiveData = MutableLiveData<ArrayList<EducationHistoryModel>>()
    val educationLiveData: LiveData<ArrayList<EducationHistoryModel>>
        get() = _educationLiveData

    fun setEducation(education: ArrayList<EducationHistoryModel>) {
        _educationLiveData.value = ArrayList(education)
    }

    private val _skillsLiveData = MutableLiveData<ArrayList<SkillModel>>()
    val skillsLiveData: LiveData<ArrayList<SkillModel>>
        get() = _skillsLiveData

    private val _languagesLiveData = MutableLiveData<ArrayList<LanguageLevelModel>>()
    val languagesLiveData: LiveData<ArrayList<LanguageLevelModel>>
        get() = _languagesLiveData

    fun setLanguages(languages: ArrayList<LanguageLevelModel>) {
        _languagesLiveData.value = languages
    }

    fun addQuote(uri: Uri) {
        _quotesLiveData.value?.let {
            val quotes = it
            quotes.add(uri)
            _quotesLiveData.value = quotes
        }
    }

    fun setWorkingExperienceImages(images: ArrayList<Uri>) {
        _workingExperienceImagesLiveData.value = images
    }

    fun setCompanyLogo(position: Int, uri: Uri) {
        val newList = _workingExperienceImagesLiveData.value
        if (position == newList?.size) {
            newList.add(uri)
        } else {
            newList!![position] = uri
        }
        _workingExperienceImagesLiveData.value = newList
    }

    fun setEducationImages(images: ArrayList<Uri>) {
        _educationImagesLiveData.value = images
    }

    fun setCertificateImage(position: Int, image: Uri) {
        val newList = _educationImagesLiveData.value
        if (position == newList?.size) {
            newList.add(image)
        } else {
            newList!![position] = image
        }
        _educationImagesLiveData.value = newList
    }

    fun removeQuote(position: Int) {
        _quotesLiveData.value?.let {
            val quotes = it
            quotes.removeAt(position)
            _quotesLiveData.value = quotes
        }
    }

    fun removeSkill(skill: String) {
        _skillsLiveData.value?.let {
            val newList = ArrayList(it)
            if (newList.contains(SkillModel(skill))) {
                newList.remove(SkillModel(skill))
                _skillsLiveData.value = newList
            }
        }
    }

    fun setSkills(skills: ArrayList<SkillModel>) {
        _skillsLiveData.value = skills
    }

    fun addEducation() {
        _educationImagesLiveData.value?.add(Uri.EMPTY)
    }

    fun addWorkingExperience() {
        _workingExperienceImagesLiveData.value?.add(Uri.EMPTY)
    }
}