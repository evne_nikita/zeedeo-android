package com.zeedeo.ui.dialog

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.zeedeo.R
import com.zeedeo.utils.Constants
import kotlinx.android.synthetic.main.dialog_language_level.*


class LanguageLevelDialog: DialogFragment() {
    private val languageLevels = arrayOf("Beginner", "Elementary", "Intermediate", "Advanced", "Fluent")

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_language_level, container, false)
    }

    override fun onResume() {
        super.onResume()
        languageLevelNumberPicker.displayedValues = languageLevels
        languageLevelNumberPicker.minValue = 0
        languageLevelNumberPicker.maxValue = 4

        okTextView.setOnClickListener {
            sendResult(languageLevels[languageLevelNumberPicker.value])
            dismiss()
        }
        cancelTextView.setOnClickListener {
            dismiss()
        }
    }

    private fun sendResult(value: String) {
        val intent = Intent()
        intent.putExtra(Constants.LANGUAGE_LEVEL_KEY, value)
        targetFragment!!.onActivityResult(
            targetRequestCode, Constants.LANGUAGE_LEVEL_REQUEST_CODE, intent
        )
    }
}