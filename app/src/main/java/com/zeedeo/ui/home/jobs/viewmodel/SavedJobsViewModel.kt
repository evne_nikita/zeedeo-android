package com.zeedeo.ui.home.jobs.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.zeedeo.model.JobModel

class SavedJobsViewModel : ViewModel() {
    private val _savedJobsLiveData = MutableLiveData(ArrayList<JobModel>())
    val savedJobLiveData: LiveData<ArrayList<JobModel>>
        get() = _savedJobsLiveData

    fun setSavedJobs(savedJobs: ArrayList<JobModel>) {
        _savedJobsLiveData.value = savedJobs
    }
}