package com.zeedeo.utils

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.firestore.ktx.toObject
import com.google.firebase.firestore.ktx.toObjects
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.zeedeo.model.*

class FirebaseUtils {

    companion object {
        const val FIRESTORE_USERS_COLLECTION = "users"
        const val FIRESTORE_USER_WORKING_EXPERIENCE_COLLECTION = "workingExperience"
        const val FIRESTORE_USER_LANGUAGES_COLLECTION = "languages"
        const val FIRESTORE_USER_EDUCATION_COLLECTION = "education"
        const val FIRESTORE_USER_SKILLS_COLLECTION = "skills"
        const val FIRESTORE_USER_ABOUT_FIELD = "about"
    }

    private val _currentUserLiveData = MutableLiveData<UserModel>()
    val currentUserLiveData: LiveData<UserModel>
        get() = _currentUserLiveData

    private val firebaseAuth = Firebase.auth
    private val firebaseStorage = Firebase.storage
    val usersStorageReference = firebaseStorage.reference.child(Constants.STORAGE_USERS_REFERENCE)
    private val firestore = Firebase.firestore

    fun signUp(newUser: UserModel) {
        firebaseAuth.createUserWithEmailAndPassword(newUser.email, newUser.password)
            .addOnSuccessListener {
                loadFirestoreData(firebaseAuth.currentUser!!.uid, newUser)
                uploadImage(
                    Uri.parse(newUser.profileImage),
                    Constants.STORAGE_PROFILE_IMAGE_PHOTO_REFERENCE,
                    Constants.STORAGE_PROFILE_IMAGE_FILE_NAME
                )
                uploadEducationImages(newUser)
                uploadWorkingExperienceImages(newUser)
                getUser()
            }
    }

    fun signIn(email: String, password: String) {
        firebaseAuth.signInWithEmailAndPassword(email, password).addOnSuccessListener {
            getUser()
        }
    }

    fun signOut() {
        firebaseAuth.signOut()
    }

    //upload image with success listener
    fun uploadImage(
        uri: Uri,
        path: String,
        name: String,
        successListener: UploadImageSuccessListener
    ) {
        firebaseAuth.currentUser?.let { user ->
            usersStorageReference.child(user.uid).child(path).child("/$name").putFile(uri)
                .addOnSuccessListener {
                    successListener.onUpload(uri)
                }
        }
    }

    fun uploadImage(uri: Uri, path: String, name: String) {
        firebaseAuth.currentUser?.let { user ->
            usersStorageReference.child(user.uid).child(path).child("/$name").putFile(uri)
        }
    }

    private fun removeImage(path: String, name: String) {
        firebaseAuth.currentUser?.let { user ->
            usersStorageReference.child(user.uid).child(path).child("/$name").delete()
        }
    }

    private fun getUser() {
        firebaseAuth.currentUser?.let { user ->
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(user.uid).get()
                .addOnSuccessListener {
                    val newUser = it.toObject<UserModel>()
                    newUser?.uid = user.uid
                    _currentUserLiveData.value = newUser
                    getWorkingExperience(user.uid)
                    getUserEducation(user.uid)
                    getUserLanguages(user.uid)
                    getUserSkills(user.uid)
                }
        }
    }

    private fun loadFirestoreData(uid: String, user: UserModel) {
        val usersCollection =
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid)
        user.uid = uid
        usersCollection.set(user)

        addCollection(
            ArrayList(user.workingExperience),
            usersCollection.collection(FIRESTORE_USER_WORKING_EXPERIENCE_COLLECTION)
        )
        addCollection(
            ArrayList(user.educationHistory),
            usersCollection.collection(FIRESTORE_USER_EDUCATION_COLLECTION)
        )
        addCollection(
            ArrayList(user.skills),
            usersCollection.collection(FIRESTORE_USER_SKILLS_COLLECTION)
        )
        addCollection(
            ArrayList(user.languages),
            usersCollection.collection(FIRESTORE_USER_LANGUAGES_COLLECTION)
        )
    }

    private fun addCollection(field: ArrayList<Any>, collection: CollectionReference) {
        for (item in field) {
            collection.add(item)
        }
    }

    fun getUserInfo(uid: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).get().addOnSuccessListener {
            val newUser = it.toObject<UserModel>()
            newUser?.uid = uid
            _currentUserLiveData.value = newUser
        }
    }

    fun uploadUserDetails(uid: String, details: UserModel) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).set(details)
            .addOnSuccessListener {
                getUserInfo(uid)
            }
    }

    fun uploadPersonalInterests(interests: String) {
        val uid = _currentUserLiveData.value!!.uid
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid)
            .update(mapOf(Constants.FIRESTORE_USER_PERSONAL_INTERESTS_FIELD to interests))
    }

    fun setUserAbout(uid: String, info: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).update(
            FIRESTORE_USER_ABOUT_FIELD,
            info
        ).addOnSuccessListener {
            getUserInfo(uid)
        }
    }

    fun getWorkingExperience(uid: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
            FIRESTORE_USER_WORKING_EXPERIENCE_COLLECTION
        ).get().addOnSuccessListener {
            val newUser = _currentUserLiveData.value
            newUser?.workingExperience = ArrayList(it.toObjects())
            _currentUserLiveData.value = newUser
        }
    }

    fun uploadWorkingExperience(uid: String, workingExperience: ArrayList<WorkingExperienceModel>) {
        // hard reset
        val workingCollection =
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
                FIRESTORE_USER_WORKING_EXPERIENCE_COLLECTION
            )
        _currentUserLiveData.value?.workingExperience?.let {
            //override existing and then add new items
            if (workingExperience.size > it.size) {
                workingCollection.get().addOnSuccessListener { query ->
                    var index = if (query.documents.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until query.documents.size) {
                        workingCollection.document(query.documents[i].id).set(workingExperience[i])
                        index = i
                    }
                    for (i in index + 1 until workingExperience.size) {
                        workingCollection.add(workingExperience[i])
                    }
                    getWorkingExperience(uid)
                }
                // override existing and remove extra items
            } else {
                workingCollection.get().addOnSuccessListener { query ->
                    var index = if (workingExperience.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until workingExperience.size) {
                        workingCollection.document(query.documents[i].id).set(workingExperience[i])
                        index = i
                    }
                    for (i in index + 1 until it.size) {
                        workingCollection.document(query.documents[i].id).delete()
                    }
                    getWorkingExperience(uid)
                }
            }
        }
    }

    fun getUserEducation(uid: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
            FIRESTORE_USER_EDUCATION_COLLECTION
        ).get().addOnSuccessListener {
            val newUser = _currentUserLiveData.value
            newUser?.educationHistory = ArrayList(it.toObjects())
            _currentUserLiveData.value = newUser
        }
    }

    fun uploadEducation(uid: String, education: ArrayList<EducationHistoryModel>) {
        // hard reset
        val educationCollection =
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
                FIRESTORE_USER_EDUCATION_COLLECTION
            )
        _currentUserLiveData.value?.educationHistory?.let {
            //override existing and then add new items
            if (education.size > it.size) {
                educationCollection.get().addOnSuccessListener { query ->
                    var index = if (query.documents.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until query.documents.size) {
                        educationCollection.document(query.documents[i].id).set(education[i])
                        index = i
                    }
                    for (i in index + 1 until education.size) {
                        educationCollection.add(education[i])
                    }
                    getUserEducation(uid)
                }
                // override existing and remove extra items
            } else {
                educationCollection.get().addOnSuccessListener { query ->
                    var index = if (education.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until education.size) {
                        educationCollection.document(query.documents[i].id).set(education[i])
                        index = i
                    }
                    for (i in index + 1 until it.size) {
                        educationCollection.document(query.documents[i].id).delete()
                    }
                    getUserEducation(uid)
                }
            }
        }
    }

    fun getUserLanguages(uid: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
            FIRESTORE_USER_LANGUAGES_COLLECTION
        ).get().addOnSuccessListener {
            val newUser = _currentUserLiveData.value
            newUser?.languages = ArrayList(it.toObjects())
            _currentUserLiveData.value = newUser
        }
    }

    fun uploadLanguages(uid: String, languages: ArrayList<LanguageLevelModel>) {
        // hard reset
        val languageCollection =
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
                FIRESTORE_USER_LANGUAGES_COLLECTION
            )
        _currentUserLiveData.value?.languages?.let {
            //override existing and then add new items
            if (languages.size > it.size) {
                languageCollection.get().addOnSuccessListener { query ->
                    var index = if (query.documents.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until query.documents.size) {
                        languageCollection.document(query.documents[i].id).set(languages[i])
                        index = i
                    }
                    for (i in index + 1 until languages.size) {
                        languageCollection.add(languages[i])
                    }
                    getUserLanguages(uid)
                }
                // override existing and remove extra items
            } else {
                languageCollection.get().addOnSuccessListener { query ->
                    var index = if (languages.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until languages.size) {
                        languageCollection.document(query.documents[i].id).set(languages[i])
                        index = i
                    }
                    for (i in index + 1 until it.size) {
                        languageCollection.document(query.documents[i].id).delete()
                    }
                    getUserLanguages(uid)
                }
            }
        }
    }

    private fun getUserSkills(uid: String) {
        firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
            FIRESTORE_USER_SKILLS_COLLECTION
        ).get().addOnSuccessListener {
            val newUser = _currentUserLiveData.value
            newUser?.skills = ArrayList(it.toObjects())
            _currentUserLiveData.value = newUser
        }
    }

    fun uploadSkills(uid: String, skills: ArrayList<SkillModel>) {
        // hard reset
        val skillsCollection =
            firestore.collection(FIRESTORE_USERS_COLLECTION).document(uid).collection(
                FIRESTORE_USER_SKILLS_COLLECTION
            )
        _currentUserLiveData.value?.skills?.let {
            //override existing and then add new items
            if (skills.size >= it.size) {
                skillsCollection.get().addOnSuccessListener { query ->
                    var index = if (query.documents.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until query.documents.size) {
                        skillsCollection.document(query.documents[i].id).set(skills[i])
                        index = i
                    }
                    for (i in index + 1 until skills.size) {
                        skillsCollection.add(skills[i])
                    }
                    getUserSkills(uid)
                }
                // override existing and remove extra items
            } else {
                skillsCollection.get().addOnSuccessListener { query ->
                    var index = if (skills.isEmpty()) {
                        -1
                    } else {
                        0
                    }
                    for (i in 0 until skills.size) {
                        skillsCollection.document(query.documents[i].id).set(skills[i])
                        index = i
                    }
                    for (i in index + 1 until it.size) {
                        skillsCollection.document(query.documents[i].id).delete()
                    }
                    getUserSkills(uid)
                }
            }
        }
    }

    private fun uploadEducationImages(user: UserModel) {
        firebaseAuth.currentUser?.let {
            for (i in 0 until user.educationHistory.size) {
                uploadImage(
                    Uri.parse(user.educationHistory[i].imageUrl),
                    Constants.STORAGE_EDUCATION_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_EDUCATION_FILE_SUFFIX}$i"
                )
            }
        }
    }

    private fun uploadWorkingExperienceImages(user: UserModel) {
        firebaseAuth.currentUser?.let {
            for (i in 0 until user.workingExperience.size) {
                uploadImage(
                    Uri.parse(user.workingExperience[i].imageUrl),
                    Constants.STORAGE_WORKING_EXPERIENCE_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_WORKING_EXPERIENCE_FILE_SUFFIX}$i"
                )
            }
        }
    }

    fun updateWorkingExperienceImages(workingExperience: ArrayList<WorkingExperienceModel>) {
        firebaseAuth.currentUser?.let {
            var removeIndex = 0
            for (i in 0 until workingExperience.size) {
                uploadImage(
                    Uri.parse(workingExperience[i].imageUrl),
                    Constants.STORAGE_WORKING_EXPERIENCE_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_WORKING_EXPERIENCE_FILE_SUFFIX}$i"
                )
                removeIndex = i
            }
            for (i in removeIndex + 1 until _currentUserLiveData.value!!.workingExperience.size) {
                removeImage(
                    Constants.STORAGE_WORKING_EXPERIENCE_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_WORKING_EXPERIENCE_FILE_SUFFIX}$i"
                )
            }
        }
    }

    fun updateEducationImages(education: ArrayList<EducationHistoryModel>) {
        firebaseAuth.currentUser?.let {
            var removeIndex = 0
            for (i in 0 until education.size) {
                uploadImage(
                    Uri.parse(education[i].imageUrl),
                    Constants.STORAGE_EDUCATION_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_EDUCATION_FILE_SUFFIX}$i"
                )
                removeIndex = i
            }
            for (i in removeIndex + 1 until _currentUserLiveData.value!!.educationHistory.size) {
                removeImage(
                    Constants.STORAGE_EDUCATION_PHOTO_REFERENCE,
                    "${it.uid}${Constants.STORAGE_EDUCATION_FILE_SUFFIX}$i"
                )
            }
        }
    }
}