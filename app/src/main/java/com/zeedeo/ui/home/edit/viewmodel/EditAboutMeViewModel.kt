package com.zeedeo.ui.home.edit.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class EditAboutMeViewModel: ViewModel() {

    private val _aboutMeLiveData = MutableLiveData("")
    val aboutMeLiveData: LiveData<String>
    get() = _aboutMeLiveData

    fun setAboutMe(text: String) {
        _aboutMeLiveData.value = text
    }
}