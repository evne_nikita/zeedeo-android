package com.zeedeo.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class LanguageLevelModel(
    @SerializedName("language")
    var language: String = "",
    @SerializedName("level")
    var level: String = "Beginner"
): Serializable